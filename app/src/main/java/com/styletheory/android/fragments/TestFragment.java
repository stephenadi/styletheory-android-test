package com.styletheory.android.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.andtinder.view.CardContainer;
import com.facebook.appevents.AppEventsLogger;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.styletheory.android.FitSizeActivity;
import com.styletheory.android.R;
import com.styletheory.android.adapter.SwipeCardAdapter;

public class TestFragment extends BaseFragment {
    public static final String PUBLISHABLE_KEY = "pk_test_6pRNASCoBOKtIshFeQd4XMUh";

    private CardContainer mCardContainer;
    private SwipeFlingAdapterView mFrame;
    private ImageView mTestImage;

    public static TestFragment newInstance(){
        TestFragment fragment = new TestFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayoutView = mLayoutInflater.inflate(R.layout.fragment_test, container, false);

        setUpView();
//        startStripe();
        setUpSwipeCard();
        setUpListener();
        return mLayoutView;
    }

    private void setUpView() {
        mTestImage = (ImageView) mLayoutView.findViewById(R.id.image_test);
        mTestImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getParentActivity(), FitSizeActivity.class);
                startActivity(i);
            }
        });
    }

    private void setUpListener() {
    }

    private void setUpSwipeCard() {
        mFrame = (SwipeFlingAdapterView) mLayoutView.findViewById(R.id.swipe_adapter_view);
        final SwipeCardAdapter adapter = new SwipeCardAdapter(mContext, R.layout.item_card, new String[]{"a","b","c","d","e"});
        mFrame.setAdapter(adapter);
        mFrame.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                // this is the simplest way to delete an object from the Adapter (/AdapterView)
                Log.d("LIST", "removed object!");
                adapter.removeFirstObject();
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
                makeToast(mContext, "Left!");
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                makeToast(getParentActivity(), "Right!");
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
                // Ask for more data here

            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                Log.d("test swipe","percent : " + scrollProgressPercent);
                View view = mFrame.getSelectedView();
                view.findViewById(R.id.frame_dislike).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.frame_like).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });

    }

    static void makeToast(Context ctx, String s){
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
    }

//    private void setUpCardContainer() {
//        mCardContainer = (CardContainer) findViewById(R.id.layoutview);
//        mCardContainer.setOrientation(Orientations.Orientation.Ordered);
//
//        Resources r = getResources();
//
//        CustomCardAdapter adapter = new CustomCardAdapter(this);
//
//        for(int i=0;i<10;i++) {
//            final CardModel cardModel = new CardModel("Title1", "Description goes here", r.getDrawable(R.drawable.picture1));
//            cardModel.setOnClickListener(new CardModel.OnClickListener() {
//                @Override
//                public void OnClickListener() {
//                    Log.i("Swipeable Cards", "I am pressing the card");
//                }
//            });
//
//            cardModel.setCardLikeImageDrawable(r.getDrawable(R.drawable.ic_launcher));
//            cardModel.setCardDislikeImageDrawable(r.getDrawable(R.drawable.camera));
//            cardModel.setOnCardDismissedListener(new CardModel.OnCardDismissedListener() {
//                @Override
//                public void onLike() {
//                    Log.i("Swipeable Cards", "I like the card");
//                }
//
//                @Override
//                public void onLikeProgress(float progress) {
//
//                }
//
//                @Override
//                public void onDisLikeProgress(float progress) {
//
//                }
//
//                @Override
//                public void onDislike() {
//                    Log.i("Swipeable Cards", "I dislike the card");
//                }
//            });
//
//            adapter.add(cardModel);
//        }
//        mCardContainer.setAdapter(adapter);
//    }

    @Override
    public void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(mContext);
    }

    @Override
    public void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(mContext);
    }


//    public void startStripe() {
//        Card card = new Card(
//                "4111111111111111",
//                10,
//                2017,
//                "123");
//
//        boolean validation = card.validateCard();
//        if (validation) {
//            Toast.makeText(mContext, "valid, starting...", Toast.LENGTH_LONG).show();
//            new Stripe().createToken(
//                    card,
//                    PUBLISHABLE_KEY,
//                    new TokenCallback() {
//                        @Override
//                        public void onError(Exception error) {
//                            Toast.makeText(mContext, "error : " + error.getMessage(), Toast.LENGTH_LONG).show();
//                        }
//
//                        @Override
//                        public void onSuccess(Token token) {
//                            Toast.makeText(mContext, "success : " + token.getId(), Toast.LENGTH_LONG).show();
//                        }
//                    });
//        } else if (!card.validateNumber()) {
//            Toast.makeText(mContext, "The card number that you entered is invalid", Toast.LENGTH_LONG).show();
//        } else if (!card.validateExpiryDate()) {
//            Toast.makeText(mContext, "The expiration date that you entered is invalid", Toast.LENGTH_LONG).show();
//        } else if (!card.validateCVC()) {
//            Toast.makeText(mContext, "The CVC code that you entered is invalid", Toast.LENGTH_LONG).show();
//        } else {
//            Toast.makeText(mContext, "The card details that you entered are invalid", Toast.LENGTH_LONG).show();
//        }
//    }
}
