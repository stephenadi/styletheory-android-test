package com.styletheory.android.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.styletheory.android.R;

public class FeatureFragment extends BaseFragment {
    public static FeatureFragment newInstance(){
        FeatureFragment fragment = new FeatureFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mLayoutView = mLayoutInflater.inflate(R.layout.fragment_feature, container, false);

        getParentActivity().setToolbarTitle("STYLE THEORY");
        setUpView();
        setUpListener();
        return mLayoutView;
    }

    private void setUpView() {
    }

    private void setUpListener() {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dummy, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
}
