package com.styletheory.android.fragments;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.JsonElement;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.styletheory.android.Constans;
import com.styletheory.android.R;
import com.styletheory.android.StyleTheoryApplication;
import com.styletheory.android.adapter.ImageAdapter;
import com.styletheory.android.api.ApiRoutes;
import com.styletheory.android.api.base.VolleyAPI;
import com.styletheory.android.api.request.WishlistRequestData;
import com.styletheory.android.api.response.CategoryResponseData;
import com.styletheory.android.api.response.DesignerResponseData;
import com.styletheory.android.api.response.ProductResponseData;
import com.styletheory.android.ui.ScrollGridView;
import com.styletheory.android.ui.TouchImageView;
import com.styletheory.android.util.FormattingUtil;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class WardrobeFragment extends BaseFragment {
    private static final int TOOLBAR_THRESHOLD = 20;

    private static ImageView mGalleryImageView;
    private boolean onLoading;
    private ScrollGridView mGridView;
    private GridAdapter mAdapter;
    private TextView mLoadingFrame;
    private boolean isLoading;
    private int mLastFirstVisible;
    private VolleyAPI<JsonElement, ProductResponseData> mProductAPI;
    private VolleyAPI<JsonElement, CategoryResponseData> mCategoryAPI;
    private ProgressDialog mLoadingDialog;
    private int mPage;
    private Integer mCategory;
    private CategoryResponseData mCategoryResponseData;
    private VolleyAPI<JsonElement, DesignerResponseData> mDesignerAPI;
    private VolleyAPI<WishlistRequestData, JsonElement> mWishListAPI;
    private DesignerResponseData mDesignerResponseData;
    private RelativeLayout mMainFrame;
    private ViewPager mGalleryFrame;
    private int mMiddlePoint;
    private static boolean mOnDrag;
    private int _yDelta;
    private float mLastX;
    private float mLastY;
    private int mInitialWidth;
    private int mInitialHeight;
    private static ImageLoader mImageLoader;

    public static WardrobeFragment newInstance() {
        WardrobeFragment fragment = new WardrobeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mLayoutView == null) {
            super.onCreateView(inflater, container, savedInstanceState);
            mLayoutView = mLayoutInflater.inflate(R.layout.fragment_main, container, false);
            onLoading = false;

            mImageLoader = StyleTheoryApplication.getImageLoader();
            getParentActivity().setToolbarTitle("CATEGORY");
            setUpView();
            setUpRequestAPI();
            setUpListener();
        } else {
            ViewGroup parentViewGroup = (ViewGroup) mLayoutView.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
        setUpAttribute();

        return mLayoutView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mLayoutView != null) {
            ViewGroup parentViewGroup = (ViewGroup) mLayoutView.getParent();
            if (parentViewGroup != null) {
                parentViewGroup.removeAllViews();
            }
        }
    }

    private void setUpAttribute() {
        mOnDrag = false;
        mGalleryImageView = null;
        Display display = getParentActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        mMiddlePoint = height / 2;
    }

    private void setUpListener() {
        mGridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int currentY = mGridView.computeVerticalScrollOffset();
                Log.d("Test", "currentY:" + currentY);
                if (mLastFirstVisible + TOOLBAR_THRESHOLD < currentY) {
                    Log.d("Test", "currentY:" + currentY);
                    getParentActivity().hideToolbar();
                    mLastFirstVisible = currentY;
                } else if (mLastFirstVisible - TOOLBAR_THRESHOLD > currentY) {
                    getParentActivity().showToolbar();
                    mLastFirstVisible = currentY;
                }

                if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                    Log.d("test", "detect end, firstvisible : " + firstVisibleItem + ", visible : " + visibleItemCount + ", total:" + totalItemCount);
                    if (!isLoading) {
                        Log.d("test", "currrently not loading");
                        loadProduct();
                    }
                }
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }
        });

        mGalleryFrame.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        mGalleryFrame.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        mGalleryFrame.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        mMainFrame.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {
                switch (dragEvent.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        Log.d("test drag", "ACTION_DRAG_STARTED");
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {//uncomment for drag along Y axis only
                            mOnDrag = true;
                            ViewGroup.MarginLayoutParams lParams = (ViewGroup.MarginLayoutParams) mGalleryImageView.getLayoutParams();
                            _yDelta = (int) dragEvent.getY() - lParams.topMargin;
                            return true;
                        }
                        return false;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        Log.d("test drag", dragEvent.getX() + "," + (dragEvent.getY() - mGridView.computeVerticalScrollOffset()));
                        Log.d("test drag", "alpha:" + (1 - (Math.abs(mMiddlePoint - (dragEvent.getY() - mGridView.computeVerticalScrollOffset())) / mMiddlePoint)));
                        mGalleryFrame.setAlpha(1 - (Math.abs(mMiddlePoint - (dragEvent.getY() - mGridView.computeVerticalScrollOffset())) / mMiddlePoint));
//uncomment for drag along Y axis only
                        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mGalleryImageView.getLayoutParams();
                        layoutParams.topMargin = (int) dragEvent.getY() - _yDelta;
                        mGalleryImageView.setLayoutParams(layoutParams);
                        return true;
                    case DragEvent.ACTION_DROP:
                        Log.d("test drag", "ACTION_DROP, alpha : " + mGalleryFrame.getAlpha());
                        if (mGalleryFrame.getAlpha() > 0.7) {
                            return false;
                        }
                        mLastX = dragEvent.getX() /*+ (mGalleryImageView.getLayoutParams().width/2)*/;
                        mLastY = dragEvent.getY() /*+ (mGalleryImageView.getLayoutParams().height/2)*/;
                        mInitialWidth = mGalleryImageView.getLayoutParams().width;
                        mInitialHeight = mGalleryImageView.getLayoutParams().height;
                        Log.d("test drag", "last x : " + mLastX + ", last y : " + mLastY);

                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:
                        mOnDrag = false;
                        mGalleryFrame.setAlpha(1);
                        if (dragEvent.getResult()) {
                            Log.d("test drag", "ACTION_DRAG_ENDED");
                            mGalleryFrame.setVisibility(View.GONE);
                            mGalleryImageView.setVisibility(View.VISIBLE);

                            int currentPost = mGalleryFrame.getCurrentItem();
                            mGalleryFrame.setAdapter(new GalleryImageAdapter(getFragmentManager()));
                            mGalleryFrame.setCurrentItem(currentPost);

                            getParentActivity().showToolbar();
//                            float oldX = mSmallImageView.getX();
//                            float oldY = mSmallImageView.getY();
//                            mSmallImageView.setX(mLastX);
//                            mSmallImageView.setY(mLastY);
//                            ObjectAnimator xAnimation = ObjectAnimator.ofFloat(mSmallImageView, "x", oldX);
//                            ObjectAnimator yAnimation = ObjectAnimator.ofFloat(mSmallImageView, "y", oldY);
//                            AnimatorSet animatorSet = new AnimatorSet();
//                            animatorSet.play(xAnimation).with(yAnimation);
//                            animatorSet.setDuration(2000);
//                            animatorSet.start();
                        } else {
                            int currentPost = mGalleryFrame.getCurrentItem();
                            mGalleryFrame.setAdapter(new GalleryImageAdapter(getFragmentManager()));
                            mGalleryFrame.setCurrentItem(currentPost);//force redraw the view pager, if not the image will be missing
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    private void setUpView() {
        mMainFrame = (RelativeLayout) mLayoutView.findViewById(R.id.frame_main);
        mGalleryFrame = (ViewPager) mLayoutView.findViewById(R.id.frame_gallery);
        mGridView = (ScrollGridView) mLayoutView.findViewById(R.id.gridview);
        mLoadingFrame = (TextView) mLayoutView.findViewById(R.id.frame_loading);

        mGalleryFrame.setAdapter(new GalleryImageAdapter(getFragmentManager()));
    }

    private void setUpRequestAPI() {
        mProductAPI = new VolleyAPI<JsonElement, ProductResponseData>(mContext) {
            @Override
            public void onRequestSuccess(ProductResponseData responseData) {
                if (mAdapter == null) {
                    mAdapter = new GridAdapter(mContext, new LinkedList<ProductResponseData.Product>(Arrays.asList(responseData.products)));
                    mGridView.setAdapter(mAdapter);
                }

                mAdapter.addAll(new LinkedList<ProductResponseData.Product>(Arrays.asList(responseData.products)));
                mPage++;
                mLoadingFrame.setVisibility(View.GONE);
                isLoading = false;
            }

            @Override
            public void onRequestError(VolleyError error) {
                mLoadingFrame.setVisibility(View.GONE);
                isLoading = false;
            }
        };

        mCategoryAPI = new VolleyAPI<JsonElement, CategoryResponseData>(mContext) {
            @Override
            public void onRequestSuccess(CategoryResponseData responseData) {
                mCategoryResponseData = responseData;
                mDesignerAPI.startRequest(ApiRoutes.DESIGNERS_URL, Constans.RequestType.GET, null);
            }

            @Override
            public void onRequestError(VolleyError error) {
                setUpRequestAPI();
            }
        };

        mDesignerAPI = new VolleyAPI<JsonElement, DesignerResponseData>(mContext) {
            @Override
            public void onRequestSuccess(DesignerResponseData responseData) {
                mDesignerResponseData = responseData;
                getParentActivity().setUpLeftNavigation(mCategoryResponseData, responseData, new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPos, long l) {
                        if (groupPos < mCategoryResponseData.categories.length) {
                            mCategory = mCategoryResponseData.categories[groupPos].id;
                            Log.d("test api", "selected id:" + mCategory + " from pos : " + groupPos);
                        }
                        return false;
                    }
                });
                mLoadingDialog.dismiss();
            }

            @Override
            public void onRequestError(VolleyError error) {
                setUpRequestAPI();
            }
        };

        mWishListAPI = new VolleyAPI<WishlistRequestData, JsonElement>(mContext) {
            @Override
            public void onRequestSuccess(JsonElement responseData) {

            }
        };

        if (mDesignerResponseData == null) {
            mLoadingDialog = ProgressDialog.show(getParentActivity(), "Loading", "Populating data...", true);
            mCategoryAPI.startRequest(ApiRoutes.CATEGORIES_URL, Constans.RequestType.GET, null);
        }
    }

    private void loadProduct() {
        isLoading = true;
        mLoadingFrame.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("per_page", "10");
        map.put("page_number", String.valueOf(mPage));
        if (mCategory != null) {
            map.put("category", String.valueOf(mCategory));
        }
        String url = FormattingUtil.getUrlStringRequest(ApiRoutes.PRODUCT_LIST_URL, map);
        mProductAPI.startRequest(url, Constans.RequestType.GET, null);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_wardrobe, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.filter) {
            getParentActivity().openRightNavigation();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class GridAdapter extends BaseAdapter {
        private final List<ProductResponseData.Product> mImageList;
        private ViewHolder mViewHolder;
        private HashMap<Integer, Boolean> tags;

        public GridAdapter(Context mContext, List<ProductResponseData.Product> test) {
            mImageList = test;
            tags = new HashMap<>();
        }

        public void addAll(List<ProductResponseData.Product> add) {
            mImageList.addAll(add);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mImageList.size();
        }

        @Override
        public ProductResponseData.Product getItem(int i) {
            return mImageList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.item_dress, parent, false);

                mViewHolder = new ViewHolder();
                mViewHolder.likeImageView = (ImageButton) convertView.findViewById(R.id.image_view_like);
                mViewHolder.designerTextView = (TextView) convertView.findViewById(R.id.text_view_designer);
                mViewHolder.priceTextView = (TextView) convertView.findViewById(R.id.text_view_price);
                mViewHolder.viewPager = (ViewPager) convertView.findViewById(R.id.viewpager);

                mViewHolder.viewPager.setAdapter(new ImageAdapter(mContext, getChildFragmentManager(), new String[]{mImageList.get(position).thumbnail}));

                final GestureDetector tapGestureDetector = new GestureDetector(getActivity(), new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent e) {
                        Log.d("test", "viewpager clicked");
                        mGalleryFrame.setVisibility(View.VISIBLE);
                        getParentActivity().hideToolbar();

//                        startActivity(new Intent(getParentActivity(), DressDetailActivity.class));
                        return false;
                    }
                });

                mViewHolder.viewPager.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View v, MotionEvent event) {
                        tapGestureDetector.onTouchEvent(event);
                        return false;
                    }
                });
                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (ViewHolder) convertView.getTag();
            }
            mViewHolder.designerTextView.setText(getItem(position).designer);
            mViewHolder.priceTextView.setText(getItem(position).retail_price);

            final ImageButton imageButton = (ImageButton) convertView.findViewById(R.id.image_view_like);

            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("test", "imageview clicked");

                    ViewHolder holder = (ViewHolder) ((View) view.getParent()).getTag();

                    Boolean tag = (Boolean) imageButton.getTag();
                    if (tag == null) {
                        if (getItem(position).wishlist) {
                            imageButton.setTag(false);
                            tags.put(position, false);
                            imageButton.setBackgroundResource(R.drawable.icon_wishlist_heart_disable);
                            mWishListAPI.startRequest(ApiRoutes.WISHLIST_URL, Constans.RequestType.DELETE, new WishlistRequestData(getItem(position).id));
                        } else {
                            imageButton.setTag(true);
                            tags.put(position, true);
                            imageButton.setBackgroundResource(R.drawable.icon_wishlist_heart);
                            mWishListAPI.startRequest(ApiRoutes.WISHLIST_URL, Constans.RequestType.POST, new WishlistRequestData(getItem(position).id));
                        }
                    } else if (!tag) {
                        imageButton.setTag(true);
                        tags.put(position, true);
                        imageButton.setBackgroundResource(R.drawable.icon_wishlist_heart);
                        mWishListAPI.startRequest(ApiRoutes.WISHLIST_URL, Constans.RequestType.POST, new WishlistRequestData(getItem(position).id));
                    } else {
                        imageButton.setTag(false);
                        tags.put(position, false);
                        imageButton.setBackgroundResource(R.drawable.icon_wishlist_heart_disable);
                        mWishListAPI.startRequest(ApiRoutes.WISHLIST_URL, Constans.RequestType.DELETE, new WishlistRequestData(getItem(position).id));
                    }
                }
            });

            Boolean tag = tags.get(position);
            if (tag == null) {
                if (getItem(position).wishlist) {
                    imageButton.setBackgroundResource(R.drawable.icon_wishlist_heart);
                } else {
                    imageButton.setBackgroundResource(R.drawable.icon_wishlist_heart_disable);
                }
            } else if (!tag) {
                imageButton.setBackgroundResource(R.drawable.icon_wishlist_heart_disable);
            } else {
                imageButton.setBackgroundResource(R.drawable.icon_wishlist_heart);
            }

            return convertView;
        }

        class ViewHolder {
            private ImageButton likeImageView;
            private TextView designerTextView;
            private TextView priceTextView;
            private ViewPager viewPager;
        }
    }

    class GalleryImageAdapter extends FragmentPagerAdapter {
        GalleryImageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment hotelImageFragment = new HotelImageFragment();
            Bundle b = new Bundle();
            hotelImageFragment.setArguments(b);
            return hotelImageFragment;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    public static class HotelImageFragment extends Fragment {
        protected View mLayoutView;
        private TouchImageView mImageView;
        private float mLastY;
        private boolean mCurrentlyIsDown;
        private int mInitialY;

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            mLayoutView = inflater.inflate(R.layout.fragment_image_gallery, container, false);

            setUpView();
            setUpListener();

            return mLayoutView;
        }

        private void setUpView() {
            mImageView = (TouchImageView) mLayoutView.findViewById(R.id.image_view_gallery);
            loadImage();
        }

        private void loadImage() {
            mImageLoader.displayImage("http://antimatter15.com/weppy/tinybrot.webp", mImageView, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    Log.d("test image", "onLoadingStarted");
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    Log.d("test image", "onLoadingFailed");
                    loadImage();
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    Log.d("test image", "onLoadingComplete");
//                    mLayoutView.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                    mImageView.invalidate();
                    mImageView.requestLayout();
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                    Log.d("test image", "onLoadingCancelled");
                }
            });
        }

        private void setUpListener() {
            mImageView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(final View view, MotionEvent motionEvent) {
                    Log.d("Test drag", "action: " + motionEvent.getAction());
                    if (mImageView.getCurrentZoom() == mImageView.getMinZoom() && motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                        Log.d("Test drag", "selisih:" + (motionEvent.getRawY() - mLastY));
                        if (!mOnDrag && ((motionEvent.getRawY() - mLastY) > 40 || (motionEvent.getRawY() - mLastY) < -40)) {
                            Log.d("Test drag", "valid for drag");
                            mGalleryImageView = mImageView;
//                            mOnDrag = true;

                            int[] location = new int[2];
                            mImageView.getLocationOnScreen(location);
                            mInitialY = location[1];

                            final ClipData clipData = ClipData.newPlainText("", "");
//                            final View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(new View(getActivity()));//uncomment to chnge to up down only

                            mImageView.resetZoom();
                            mImageView.startDrag(clipData, shadowBuilder, view, 0);
//                            mImageView.setVisibility(View.INVISIBLE);//comment to chnge to up down only
                            return true;//return true mean the other touch listener wont get a chance to process it
                        } else {
                            Log.d("Test drag", "invalid for drag, mOndrag : " + mOnDrag);
                        }
                    } else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        mCurrentlyIsDown = false;
                        mLastY = motionEvent.getRawY();
                        Log.d("Test drag", "down y:" + motionEvent.getRawY());
                    }

                    return false;//return false mean the other touch listener may get chance to process it
                }
            });
        }
    }
}
