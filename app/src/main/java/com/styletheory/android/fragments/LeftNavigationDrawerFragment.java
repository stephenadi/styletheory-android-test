package com.styletheory.android.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.styletheory.android.R;
import com.styletheory.android.api.response.CategoryResponseData;
import com.styletheory.android.api.response.DesignerResponseData;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by stephen on 11/23/15.
 */

public class LeftNavigationDrawerFragment extends Fragment {
    public static final int WHATS_NEW_SECTION = 0;
    public static final int POPULAR_SECTION = 1;
    public static final int DESIGNER_SECTION = 2;
    public static final int FAVOURITES_SECTION = 3;
    public static final int TOPS_SECTION = 4;
    public static final int BOTTOMS_SECTION = 5;

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * A pointer to the current callbacks instance (the Activity).
     */
    private NavigationDrawerCallbacks mCallbacks;

    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    private View mDrawerView;
    private View mFragmentContainerView;

    private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;
    private ExpandableListView mListView;

    public LeftNavigationDrawerFragment() {
    }

    //================================================================================
    // Life Cycle
    //================================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
        // Select either the default item (0) or the last selected item.
        selectItem(mCurrentSelectedPosition);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mDrawerView = (View) inflater.inflate(R.layout.fragment_left_navigation_drawer, container, false);

        setUpView();
        return mDrawerView;
    }

    private void setUpView() {
        mListView = (ExpandableListView) mDrawerView.findViewById(R.id.list_view_left_navigation);
//        mListView.setGroupIndicator(getResources().getDrawable(R.drawable.selector_left_navigation));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            showGlobalContextActionBar();
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //================================================================================
    // Action Bar
    //================================================================================

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        hiddenKeyboard();

//        ActionBar actionBar = getActionBar();
//        actionBar.setDisplayShowTitleEnabled(true);
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
//        actionBar.setTitle(R.string.app_name);
//        actionBar.setSubtitle(null);
//        actionBar.setIcon(R.drawable.tororo_logo);

    }

    private ActionBar getActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                toolbar,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close   /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                selectItem();
                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(mDrawerToggle);
        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void setUpLeftNavigation(CategoryResponseData responseData, DesignerResponseData designerResponseData, ExpandableListView.OnGroupClickListener onGroupClickListener) {
        ArrayList<String> title = new ArrayList<>();
        LinkedHashMap<String, ArrayList<String>> body = new LinkedHashMap<>();
        for(int i=0;i<responseData.categories.length;i++){
            title.add(responseData.categories[i].name);
            body.put(responseData.categories[i].name, new ArrayList<String>());
        }
        title.add("DESIGNER");

        ArrayList<String> designer = new ArrayList<>();
        for(int i=0;i<designerResponseData.designers.length;i++){
            designer.add(designerResponseData.designers[i].name);
        }
        body.put("DESIGNER", designer);

//        title.add("WHAT'S NEW");
//        title.add("POPULAR");
//        title.add("TOPS");
//        title.add("BOTTOMS");
//        title.add("DESIGNER");
//
//        ArrayList<String> tops = new ArrayList<>();
//        tops.add("top 1");
//        tops.add("top 2");
//        tops.add("top 3");
//
//        ArrayList<String> bottoms = new ArrayList<>();
//        bottoms.add("bottom 1");
//        bottoms.add("bottom 2");
//
//        ArrayList<String> designer = new ArrayList<>();
//        designer.add("Ivan Gunawan");
//        designer.add("Andrew Garf");
//        designer.add("Steve Ewon");
//        designer.add("Sarah Saras");
//        designer.add("Isyana Maulan");
//        designer.add("Taylor Swift");
//
//        body.put("WHAT'S NEW", new ArrayList<String>());
//        body.put("POPULAR", new ArrayList<String>());
//        body.put("TOPS", tops);
//        body.put("BOTTOMS", bottoms);
//        body.put("DESIGNER", designer);
        mListView.setOnGroupClickListener(onGroupClickListener);
        mListView.setAdapter(new ExpandableListAdapter(getActivity(), title, body));
    }

    private void selectItem() {
        selectItem(mCurrentSelectedPosition);
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    public void toggleDrawer() {
        if (mDrawerLayout.isDrawerOpen(mFragmentContainerView)) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        } else {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }
    }

    public void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }


    //================================================================================
    // Called From Activity
    //================================================================================

    public void setDrawerIndicatorEnabled(boolean enabled) {
        mDrawerToggle.setDrawerIndicatorEnabled(enabled);
    }

    public void openRightDrawer() {
        mDrawerLayout.openDrawer(Gravity.RIGHT);
    }


    //================================================================================
    // Interface Callback
    //================================================================================

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }

    protected void hiddenKeyboard() {
        if (getView() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }

    private class ExpandableListAdapter extends BaseExpandableListAdapter {
        private Context mContext;
        private List<String> mHeaderList;
        private LinkedHashMap<String, ArrayList<String>> mCompleteData;

        public ExpandableListAdapter(Context context, List<String> listDataHeader, LinkedHashMap<String, ArrayList<String>> listChildData) {
            this.mContext = context;
            this.mHeaderList = listDataHeader;
            this.mCompleteData = listChildData;
        }

        @Override
        public String getChild(int groupPosition, int childPosititon) {
            return this.mCompleteData.get(this.mHeaderList.get(groupPosition)).get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final String childText = getChild(groupPosition, childPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.item_left_navigation_child, null);
            }

            TextView txtListChild = (TextView) convertView.findViewById(R.id.text_view_child);

            txtListChild.setText(childText);
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this.mCompleteData.get(this.mHeaderList.get(groupPosition)).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this.mHeaderList.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this.mHeaderList.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.item_left_navigation_parent, null);
            }

            TextView lblListHeader = (TextView) convertView.findViewById(R.id.text_view_title);
            ImageView minusImageView = (ImageView) convertView.findViewById(R.id.image_view_minus);
            ImageView plusImageView = (ImageView) convertView.findViewById(R.id.image_view_plus);
            if (getChildrenCount(groupPosition) == 0) {
                minusImageView.setVisibility(View.GONE);
                plusImageView.setVisibility(View.GONE);
            } else {
                if (isExpanded) {
                    minusImageView.setVisibility(View.GONE);
                    plusImageView.setVisibility(View.VISIBLE);
                } else {
                    plusImageView.setVisibility(View.GONE);
                    minusImageView.setVisibility(View.VISIBLE);
                }
            }

            lblListHeader.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
}