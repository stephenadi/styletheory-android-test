package com.styletheory.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.styletheory.android.ui.form.FormContract;
import com.styletheory.android.ui.form.SizeLayout;

import java.util.ArrayList;

/**
 * Created by stephen on 11/10/15.
 */
public class FitSizeActivity extends Activity {
    private Button mContinueButton;
    private SizeLayout mDressFrame;
    private SizeLayout mJeansFrame;
    private SizeLayout mTopFrame;
    private SizeLayout mSkirtFrame;
    private TextView mSkipTextView;
    private TextView mBackTextView;
    private ArrayList<FormContract> mFormFields;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_size_fit);

        setUpAttribute();
        setUpView();
        setUpViewState();
        setUpListener();
    }

    private void setUpAttribute() {
        mFormFields = new ArrayList<FormContract>();
    }

    private void setUpView() {
        mDressFrame = (SizeLayout) findViewById(R.id.frame_dress);
        mJeansFrame = (SizeLayout) findViewById(R.id.frame_jeans);
        mTopFrame = (SizeLayout) findViewById(R.id.frame_tops);
        mSkirtFrame = (SizeLayout) findViewById(R.id.frame_skirt);
        mContinueButton = (Button) findViewById(R.id.button_continue);
        mSkipTextView = (TextView) findViewById(R.id.text_view_skip);
        mBackTextView = (TextView) findViewById(R.id.text_view_back);
    }

    private void setUpViewState() {
        ArrayList<String> dress = new ArrayList<>();
        dress.add("0");
        dress.add("2");
        dress.add("4");
        dress.add("6");
        dress.add("8");
        dress.add("10");
        dress.add("12");
        dress.add("14");
        dress.add("16");
        mDressFrame.inflateLayout("dress", dress);
        mFormFields.add(mDressFrame);

        ArrayList<String> tops = new ArrayList<>();
        tops.add("XS");
        tops.add("S");
        tops.add("M");
        tops.add("L");
        tops.add("XL");
        tops.add("XXL");
        mTopFrame.inflateLayout("tops", tops);
        mFormFields.add(mTopFrame);

        ArrayList<String> jeans = new ArrayList<>();
        jeans.add("25");
        jeans.add("26");
        jeans.add("27");
        jeans.add("28");
        jeans.add("29");
        jeans.add("30");
        jeans.add("31");
        jeans.add("32");
        mJeansFrame.inflateLayout("jeans", jeans);
        mFormFields.add(mJeansFrame);

        ArrayList<String> skirt = new ArrayList<>();
        skirt.add("0");
        skirt.add("2");
        skirt.add("4");
        skirt.add("6");
        skirt.add("8");
        skirt.add("10");
        skirt.add("12");
        skirt.add("14");
        skirt.add("16");
        mSkirtFrame.inflateLayout("skirt", skirt);
        mFormFields.add(mSkirtFrame);
    }

    private void setUpListener() {
        mSkipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(FitSizeActivity.this, SwipeActivity.class);
                Intent i = new Intent(FitSizeActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        mBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(allFormValid()) {
//                    Intent i = new Intent(FitSizeActivity.this, SwipeActivity.class);
                    Intent i = new Intent(FitSizeActivity.this, MainActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    private boolean allFormValid() {
        for(FormContract formContract : mFormFields){
            if(formContract.getValue() == null){
                Toast.makeText(this, "Please complete all form", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }
}
