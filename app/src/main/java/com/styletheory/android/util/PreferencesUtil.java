package com.styletheory.android.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by stephen on 11/19/15.
 */
public class PreferencesUtil {
    private static final String contextPrefFile = "com.styletheory.android.preferences";
    private static final String token = "token";

    public static void setToken(Context mContext, String token) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(PreferencesUtil.contextPrefFile, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PreferencesUtil.token, token);
        editor.commit();
    }

    public static String getToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(PreferencesUtil.contextPrefFile, Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(PreferencesUtil.token, null);
        return token;
    }
}
