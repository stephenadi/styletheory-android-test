package com.styletheory.android.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by stephen on 12/15/15.
 */
public class FormattingUtil {
    public static String getUrlStringRequest(String baseUrl, HashMap<String, String> parameter){
        baseUrl += "?";
        boolean isFirst = true;
        for (Map.Entry<String, String> entry : parameter.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            value = value.replace(" ", "%20");
            if(isFirst) {
                baseUrl += key + "=" + value;
                isFirst = false;
            } else {


                baseUrl += "&" + key + "=" + value;
            }
        }

        return baseUrl;
    }
}
