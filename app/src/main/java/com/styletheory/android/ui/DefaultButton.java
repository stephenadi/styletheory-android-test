package com.styletheory.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.Button;

import com.styletheory.android.R;
import com.styletheory.android.ui.font.FontChangeHelper;

/**
 * Created by stephen on 11/26/15.
 */
public class DefaultButton extends Button {
    private int backgroundColor = getContext().getResources().getColor(R.color.pink_edit_text);
    private int textColor = getContext().getResources().getColor(R.color.white);
    private String selectedFont = "circe.otf";


    public DefaultButton(Context context) {
        super(context);
        decorTextView(context);
    }

    public DefaultButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        decorTextView(context);
    }

    public DefaultButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        decorTextView(context);
    }

    public void decorTextView(Context context) {
        FontChangeHelper.init(context);
        FontChangeHelper.setCustomFont(this, selectedFont);

        this.setBackgroundColor(backgroundColor);
        this.setTextColor(textColor);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
