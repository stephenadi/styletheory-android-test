package com.styletheory.android.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckedTextView;

import com.styletheory.android.R;
import com.styletheory.android.ui.font.FontChangeHelper;

/**
 * Created by stephen on 11/10/15.
 */
public class SizeTextView extends CheckedTextView {
    private String selectedFont = "circe.otf";

    public SizeTextView(Context context) {
        super(context);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });
    }

    public SizeTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.SizeTextView, defStyle, 0);

        if(a.hasValue(R.styleable.SizeTextView_text)){
            setText(a.getString(R.styleable.SizeTextView_text));
        }
        a.recycle();

        decorTextView(context);
    }

    public SizeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.SizeTextView);

        if(a.hasValue(R.styleable.SizeTextView_text)){
            setText(a.getString(R.styleable.SizeTextView_text));
        }
        a.recycle();

        decorTextView(context);
    }

    public void decorTextView(Context context) {
        FontChangeHelper.init(context);
        FontChangeHelper.setCustomFont(this, selectedFont);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth()); //Snap to width
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, w, oldw, oldh);
    }
}
