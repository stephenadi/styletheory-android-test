package com.styletheory.android.ui.form;

/**
 * Created by stephen on 12/3/15.
 */
public interface FormContract {
    String getKey();
    String getValue();
}
