package com.styletheory.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.Button;

import com.styletheory.android.ui.font.FontChangeHelper;

/**
 * Created by stephen on 11/26/15.
 */
public class BaseDefaultButton extends Button {
    private String selectedFont = "circe.otf";

    public BaseDefaultButton(Context context) {
        super(context);
        decorTextView(context);
    }

    public BaseDefaultButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        decorTextView(context);
    }

    public BaseDefaultButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        decorTextView(context);
    }

    public void decorTextView(Context context) {
        FontChangeHelper.init(context);
        FontChangeHelper.setCustomFont(this, selectedFont);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
