package com.styletheory.android.ui.form;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.styletheory.android.ui.font.FontChangeHelper;

/**
 * Created by stephen on 12/3/15.
 */
public class CustomEditText extends EditText implements FormContract {
    private String selectedFont = "circe.otf";
    private String mKey;

    public CustomEditText(Context context) {
        super(context);
        decorTextView(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        decorTextView(context);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        decorTextView(context);
    }

    public CustomEditText(Context context, String mKey) {
        super(context);
        this.mKey = mKey;
    }

    public void decorTextView(Context context) {
        FontChangeHelper.init(context);
        FontChangeHelper.setCustomFont(this, selectedFont);
    }

    @Override
    public String getKey() {
        return mKey;
    }

    @Override
    public String getValue() {
        return getText().toString();
    }
}
