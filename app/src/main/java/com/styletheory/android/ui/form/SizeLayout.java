package com.styletheory.android.ui.form;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.styletheory.android.R;
import com.styletheory.android.util.AnimationUtil;

import java.util.ArrayList;

/**
 * Created by stephen on 12/2/15.
 */
public class SizeLayout extends LinearLayout implements FormContract {
    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private int mContainerWidth;
    private double eachWidth;
    private ArrayList<LinearLayout> mContainerList;
    private LinearLayout currentContainer;
    private String mKey;
    private int mTotalColumn = 5;

    public SizeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mContainerWidth = size.x;
        eachWidth = (0.8* mContainerWidth) / mTotalColumn;
    }

    public void inflateLayout(String key, final ArrayList<String> value){
        inflateLayout(key, value, false);
    }

    public void inflateLayout(String key, final ArrayList<String> value, boolean stretchWidth) {
        inflateLayout(key, value, stretchWidth, 5, R.layout.item_size, mContainerWidth);
    }

    public void inflateLayout(String key, final ArrayList<String> value, boolean stretchWidth, int totalColumn, int resourceId, int containerWidth) {
        mKey = key;
        mTotalColumn = totalColumn;
        this.mContainerWidth = containerWidth;

        if(stretchWidth){
            eachWidth = (0.8* mContainerWidth) / Math.min(value.size(), mTotalColumn);
        }

        mContainerList = new ArrayList<>();

        for (int i = 0; i < value.size(); i++) {
            if (i % mTotalColumn == 0) {
                currentContainer = new LinearLayout(mContext);
                currentContainer.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                currentContainer.setOrientation(HORIZONTAL);
                currentContainer.setGravity(Gravity.CENTER);
                mContainerList.add(currentContainer);
                addView(currentContainer);
            }
            TextView textView = (TextView) mLayoutInflater.inflate(resourceId, currentContainer, false);
            textView.setText(value.get(i));
            currentContainer.addView(textView);

            LayoutParams lp = new LayoutParams((int) eachWidth, LayoutParams.WRAP_CONTENT, 0);
            textView.setLayoutParams(lp);
            lp.setMargins(AnimationUtil.convertDpToPixel(5, mContext),
                    AnimationUtil.convertDpToPixel(3, mContext),
                    AnimationUtil.convertDpToPixel(5, mContext),
                    AnimationUtil.convertDpToPixel(3, mContext));

            final int finalI = i;
            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    uncheckedAllText(value.get(finalI));
                }
            });
            textView.requestLayout();
        }
    }

    private void uncheckedAllText(String text) {
        for (int i = 0; i < mContainerList.size(); i++) {
            LinearLayout current = mContainerList.get(i);
            for (int j = 0; j < current.getChildCount(); j++) {
                if (current.getChildAt(j) instanceof CheckedTextView) {
                    final CheckedTextView textView = (CheckedTextView) current.getChildAt(j);
                    if (!textView.getText().equals(text)) {
                        textView.setChecked(false);
                    } else {
                        textView.setChecked(true);
                    }
                }
            }
        }
    }

    @Override
    public String getKey() {
        return mKey;
    }

    @Override
    public String getValue(){
        for (int i = 0; i < mContainerList.size(); i++) {
            LinearLayout current = mContainerList.get(i);
            for (int j = 0; j < current.getChildCount(); j++) {
                if (current.getChildAt(j) instanceof CheckedTextView) {
                    final CheckedTextView textView = (CheckedTextView) current.getChildAt(j);
                    if (textView.isChecked()) {
                        return textView.getText().toString();
                    }
                }
            }
        }
        return null;
    }
}

