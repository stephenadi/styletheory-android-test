package com.styletheory.android.ui.font;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by stephen on 11/26/15.
 */

public class CirceLight extends TextView {
    private String selectedFont = "circe_light.otf";

    public CirceLight(Context context) {
        super(context);
        decorTextView(context);
    }

    public CirceLight(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        decorTextView(context);
    }

    public CirceLight(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        decorTextView(context);
    }

    public void decorTextView(Context context) {
        FontChangeHelper.init(context);
        FontChangeHelper.setCustomFont(this, selectedFont);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
