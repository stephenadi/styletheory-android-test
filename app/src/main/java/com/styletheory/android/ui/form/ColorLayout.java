package com.styletheory.android.ui.form;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.styletheory.android.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by stephen on 12/7/15.
 */
public class ColorLayout extends LinearLayout{
    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private int mContainerWidth;
    private double eachWidth;
    private ArrayList<LinearLayout> mContainerList;
    private LinearLayout currentContainer;
    private int mTotalColumn = 5;

    public ColorLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOrientation(VERTICAL);
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        mContainerWidth = size.x;
        eachWidth = (0.8* mContainerWidth) / mTotalColumn;
    }

    public void inflateLayout(final ArrayList<String> value){
        inflateLayout(value, false);
    }

    public void inflateLayout(final ArrayList<String> value, boolean stretchWidth) {
        inflateLayout(value, stretchWidth, 5, mContainerWidth);
    }

    public void inflateLayout( final ArrayList<String> value, boolean stretchWidth, int totalColumn, int containerWidth) {
        mTotalColumn = totalColumn;
        this.mContainerWidth = containerWidth;

        if(stretchWidth){
            eachWidth = (0.8* mContainerWidth) / Math.min(value.size(), mTotalColumn);
        }

        mContainerList = new ArrayList<>();

        for (int i = 0; i < value.size(); i++) {
            if (i % mTotalColumn == 0) {
                currentContainer = new LinearLayout(mContext);
                currentContainer.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                currentContainer.setOrientation(HORIZONTAL);
                currentContainer.setGravity(Gravity.CENTER);
                mContainerList.add(currentContainer);
                addView(currentContainer);
            }
            CircleImageView imageView = (CircleImageView) mLayoutInflater.inflate(R.layout.item_filter_color, currentContainer, false);
            Drawable myIcon = getResources().getDrawable(R.drawable.drawable_image_color);
            myIcon.setColorFilter(Color.parseColor(value.get(i)), PorterDuff.Mode.SRC_ATOP);

            currentContainer.addView(imageView);

//            LayoutParams lp = imageView.getLayoutParams();
//            lp.setMargins(AnimationUtil.convertDpToPixel(5, mContext),
//                    AnimationUtil.convertDpToPixel(3, mContext),
//                    AnimationUtil.convertDpToPixel(5, mContext),
//                    AnimationUtil.convertDpToPixel(3, mContext));
            imageView.setImageDrawable(myIcon);
            imageView.setTag(value.get(i));
//            imageView.setBackgroundColor(Color.parseColor(value.get(i)));

            final int finalI = i;
            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    uncheckedAllText(value.get(finalI));
                }
            });
            imageView.requestLayout();
        }
    }

    private void uncheckedAllText(String text) {
        for (int i = 0; i < mContainerList.size(); i++) {
            LinearLayout current = mContainerList.get(i);
            for (int j = 0; j < current.getChildCount(); j++) {
                if (current.getChildAt(j) instanceof CircleImageView) {
                    final CircleImageView imageView = (CircleImageView) current.getChildAt(j);
                    if (!imageView.getTag().equals(text)) {
                        imageView.setBorderColorResource(android.R.color.transparent);
                    } else {
                        imageView.setBorderColorResource(R.color.white);
                    }
                }
            }
        }
    }
}


