package com.styletheory.android.ui.font;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by stephen on 11/26/15.
 */

public class FontChangeHelper {
    private static FontChangeHelper instance;
    private static Activity activity;
    private static Context context;

    public FontChangeHelper(Activity activity) {
        this.activity = activity;
        context = activity.getApplicationContext();
    }

    public FontChangeHelper(Context context) {
        this.context = context;
    }

    public static FontChangeHelper init(Activity activity) {
        if (instance == null) {
            instance = new FontChangeHelper(activity);
        }
        return instance;
    }

    public static FontChangeHelper init(Context context) {
        if (instance == null) {
            instance = new FontChangeHelper(context);
        }
        return instance;
    }

    public static void setCustomFont(TextView textView, String fontName) {
        Typeface tf = FontCache.get(fontName, context);
        if (tf != null) {
            textView.setTypeface(tf);
        }
    }
}
