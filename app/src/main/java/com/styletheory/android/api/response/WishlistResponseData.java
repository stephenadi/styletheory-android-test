package com.styletheory.android.api.response;

/**
 * Created by stephen on 12/15/15.
 */
public class WishlistResponseData {
    public Wishlist[] wishlist;

    public static class Wishlist {
        public int product_id;
        public String name;
        public String designer;
        public String thumbnail;
        public String retail_price;
    }
}
