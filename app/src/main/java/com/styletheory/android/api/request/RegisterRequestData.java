package com.styletheory.android.api.request;

/**
 * Created by stephen on 12/15/15.
 */
public class RegisterRequestData {
    public String email;
    public String password;
    public String grant_type;
    public String code;
}
