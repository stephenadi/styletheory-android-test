package com.styletheory.android.api.base;

import android.content.ContentResolver;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;

/**
 * Created by stephen on 1/6/15.
 */
public class BaseAPI {

    //================================================================================
    // Base Constant
    //================================================================================

    public String TAG = "BaseAPI";

    public static final String BASE_MOBILE_URL = "m.traveloka.com";
    public static final String BASE_MOBILE_HTTPS_URL = "https://" + BASE_MOBILE_URL;

    protected static final String BASE_API_URL = "api.traveloka.com";

    protected static final String BASE_VERSION = "/v1";
    protected static final String BASE_API_HTTP_URL = "http://" + BASE_API_URL + BASE_VERSION;
    protected static final String BASE_API_HTTPS_URL = "https://" + BASE_API_URL + BASE_VERSION;


    //================================================================================
    // API Routes
    //================================================================================

    protected static final String API_CONTEXT_ROUTES = "/user/context";
    protected static final String API_SESSION_ROUTES = "/user/session";
    protected static final String API_USER_DEVICE_INFO_ROUTES = "/user/device/info";

    protected static final String API_GEO_DATA_INFO_COUNTRIES_ROUTES = "/geo/data/infocountries";

    /**
     * Hotel Search
     */
    protected static final String API_HOTEL_AUTOCOMPLETE_ROUTES = "/hotel/autocomplete";
    protected static final String API_HOTEL_SEARCH_DATA_ROUTES = "/hotel/search/data";
    protected static final String API_HOTEL_SEARCH_INVENTORY_ROUTES = "/hotel/search/inventory";
    protected static final String API_HOTEL_OMNIBOX_ROUTES = "/hotel/omnibox/get";
    protected static final String API_HOTEL_DETAIL_ROUTES = "/hotel/search/detail";
    protected static final String API_HOTEL_REVIEW_ROUTES = "/hotel/hotelReviewAggregate";
    protected static final String API_HOTEL_ROOM_ROUTES = "/hotel/search/room";
    protected static final String API_HOTEL_PRE_BOOKING_ROUTES = "/hotel/book/preBooking";
    protected static final String API_HOTEL_CREATE_BOOKING_ROUTES = "/hotel/book/create";
    protected static final String API_BOOKING_INFO_ROUTES = "/booking/info";
    protected static final String API_HOTEL_VOUCHER_INFO_ROUTES = "/hotel/voucherinfo";


    /**
     * Hotel Search
     */
    protected static final String API_HOTEL_AUTOCOMPLETE_URL = BASE_API_HTTP_URL + API_HOTEL_AUTOCOMPLETE_ROUTES;
    protected static final String API_HOTEL_SEARCH_DATA_URL = BASE_API_HTTP_URL + API_HOTEL_SEARCH_DATA_ROUTES;
    protected static final String API_HOTEL_SEARCH_INVENTORY_URL = BASE_API_HTTP_URL + API_HOTEL_SEARCH_INVENTORY_ROUTES;
    protected static final String API_HOTEL_OMNIBOX_URL = BASE_API_HTTP_URL + API_HOTEL_OMNIBOX_ROUTES;
    protected static final String API_HOTEL_DETAIL_URL = BASE_API_HTTPS_URL + API_HOTEL_DETAIL_ROUTES;
    protected static final String API_HOTEL_ROOM_URL = BASE_API_HTTPS_URL + API_HOTEL_ROOM_ROUTES;
    protected static final String API_HOTEL_REVIEW_URL = BASE_API_HTTPS_URL + API_HOTEL_REVIEW_ROUTES;

    /**
     * Hotel Booking
     */
    protected static final String API_HOTEL_PRE_BOOKING_URL = BASE_API_HTTPS_URL + API_HOTEL_PRE_BOOKING_ROUTES;
    protected static final String API_HOTEL_CREATE_BOOKING_URL = BASE_API_HTTPS_URL + API_HOTEL_CREATE_BOOKING_ROUTES;
    protected static final String API_HOTEL_VOUCHER_INFO_URL = BASE_API_HTTPS_URL + API_HOTEL_VOUCHER_INFO_ROUTES;


    //================================================================================
    // Base Class Variable, Constructor, and Listener
    //================================================================================

    protected Context mContext;
    protected RequestQueue mRequestQueue;
    protected ContentResolver mResolver;

    protected OnBaseResponseListener mOnBaseResponseListener;

    public BaseAPI(Context context) {
        mContext = context;
        mResolver = mContext.getContentResolver();

        mRequestQueue = RequestManager.getRequestQueue();
    }

    public interface OnBaseResponseListener {
        public void onRequestError(VolleyError volleyError);

        public void onRequestFailed(String message);
    }

    //================================================================================
    // Common function for API
    //================================================================================

    public void setOnResponseListener(OnBaseResponseListener onBaseResponseListener) {
        mOnBaseResponseListener = onBaseResponseListener;
    }
}
