package com.styletheory.android.api;

/**
 * Created by stephen on 12/15/15.
 */
public class ApiRoutes {
    public static final String BASE_URL = "http://private-4e6a57-styletheory.apiary-mock.com/v1/";
    public static final String REGISTER_URL = BASE_URL + "user";
    public static final String THIRD_PARTY_LOGIN_URL = BASE_URL + "access_token/open_id";
    public static final String LOGIN_URL = BASE_URL + "access_token";
    public static final String APP_INSTANCE_URL = BASE_URL + "app_instance";
    public static final String CATEGORIES_URL = BASE_URL + "categories";
    public static final String PRODUCT_LIST_URL = BASE_URL + "products";
    public static final String DESIGNERS_URL = BASE_URL + "designers";
    public static final String WISHLIST_URL = BASE_URL + "user/wishlist";
}
