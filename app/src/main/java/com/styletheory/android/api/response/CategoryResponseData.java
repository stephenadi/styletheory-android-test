package com.styletheory.android.api.response;

/**
 * Created by stephen on 12/15/15.
 */
public class CategoryResponseData {
    public Category[] categories;

    public static class Category {
        public int id;
        public String name;
    }
}
