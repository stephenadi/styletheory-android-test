package com.styletheory.android.api.base;

import android.content.ContentResolver;
import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.styletheory.android.BuildConfig;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by stephen on 7/15/15.
 */

public abstract class VolleyAPI <P,R> implements ApiInterface<P,R> {
    public String TAG = "BaseAPI";

    protected Context mContext;
    protected RequestQueue mRequestQueue;
    protected ContentResolver mResolver;
    protected int mTotalRetry;
    protected int mMaxRetry;
    protected String mUrl;
    protected int mRequestType;
    protected P mRequestData;

    public VolleyAPI(Context context) {
        mContext = context;
        mTotalRetry = 0;
        mResolver = mContext.getContentResolver();

        mRequestQueue = RequestManager.getRequestQueue();
    }

    @Override
    public void onRequestFailed(String message) {
        if(mTotalRetry < mMaxRetry) {
            mTotalRetry++;
            Log.d("Test retry", "retry fail " + mTotalRetry + " of " + mMaxRetry);
            restartRequest(mUrl, mRequestType, mRequestData);
        }
    }

    @Override
    public void onRequestError(VolleyError error) {
        if(mTotalRetry < mMaxRetry) {
            mTotalRetry++;
            restartRequest(mUrl, mRequestType, mRequestData);
        }
    }

    @Override
    public void cancelRequest() {
        if(mUrl != null) {
            mRequestQueue.cancelAll(mUrl);
        }
    }

    @Override
    public void startRequest(String url, int requestType, P requestData) {
        startRequest(url, requestType, requestData, 0);
    }

    @Override
    public void startRequest(String url, int requestType, P requestData, int maxRetry){
        startRequest(url,requestType,requestData, maxRetry, false);
    }

    private void restartRequest(String url, int requestType, P requestData) {
        startRequest(url,requestType,requestData, mMaxRetry, true);
    }

    private void startRequest(final String url, final int requestType, final P requestData, final int maxRetry, boolean isRetry) {
        mMaxRetry = maxRetry;
        mUrl = url;
        mRequestType = requestType;
        mRequestData = requestData;
        Log.d("Test url", "url : " + mUrl);

        if(!isRetry){
            mTotalRetry = 0;
        }

        Gson gson = new Gson();
        String requestBody = gson.toJson(requestData);

        if (BuildConfig.DEBUG) {
            Class<P> requestClass = (Class<P>) ((ParameterizedType) VolleyAPI.this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];

            Log.v(requestClass.getSimpleName(), requestBody);
        }

        final Object responseClass;
        Object responseClass1;
        try {
            responseClass1 = (Class<R>) ((ParameterizedType) VolleyAPI.this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        } catch (ClassCastException ex){
            responseClass1 = ((ParameterizedType) VolleyAPI.this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
        }

        responseClass = responseClass1;
        BaseRequest stringRequest = new BaseRequest(mContext, requestType, url, requestBody, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (BuildConfig.DEBUG) {
                    if (response.length() > 4000) {
                        int chunkCount = response.length() / 4000;     // integer division
                        for (int i = 0; i <= chunkCount; i++) {
                            int max = 4000 * (i + 1);
                            if (max >= response.length()) {
                                Log.v(responseClass.toString(), "chunk " + i + " of " + chunkCount + ":" + response.substring(4000 * i));
                            } else {
                                Log.v(responseClass.toString(), "chunk " + i + " of " + chunkCount + ":" + response.substring(4000 * i, max));
                            }
                        }
                    } else {
                        Log.v(responseClass.toString(), response);
                    }
                }


                try {
                    Gson gson = new Gson();
                    R responseData = gson.fromJson(response, (Type) responseClass);
                    onRequestSuccess(responseData);
                } catch (Exception e) {
                    e.printStackTrace();
                    onRequestFailed("error parsing json");
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                volleyError.printStackTrace();
                onRequestError(volleyError);
            }
        });

        mRequestQueue.cancelAll(mUrl);
        stringRequest.setTag(mUrl);
        mRequestQueue.add(stringRequest);
    }
}
