package com.styletheory.android.api.request;

/**
 * Created by stephen on 12/15/15.
 */
public class LoginRequestData {
    public String response_type;
    public String username;
    public String password;
    public String grant_type;
    public String client_id;
    public String client_secret;
}
