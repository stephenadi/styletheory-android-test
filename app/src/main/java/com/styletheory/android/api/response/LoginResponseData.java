package com.styletheory.android.api.response;

/**
 * Created by stephen on 12/15/15.
 */
public class LoginResponseData {
    public String access_token;
    public String token_type;
    public long expires_in;
    public String refresh_token;
}
