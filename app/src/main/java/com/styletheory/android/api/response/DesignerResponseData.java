package com.styletheory.android.api.response;

/**
 * Created by stephen on 12/15/15.
 */
public class DesignerResponseData {
    public Designer[] designers;

    public static class Designer{
        public int id;
        public String name;
    }
}
