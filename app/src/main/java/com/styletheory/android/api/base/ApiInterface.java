package com.styletheory.android.api.base;

import com.android.volley.VolleyError;

/**
 * Created by stephen on 7/15/15.
 */
public interface ApiInterface<P,R> {
    void onRequestSuccess(R responseData);
    void onRequestFailed(String message);
    void onRequestError(VolleyError error);
    void startRequest(String url, int requestType, P requestData);
    void startRequest(String url, int requestType, P requestData, int maxRetry);
    void cancelRequest();
}
