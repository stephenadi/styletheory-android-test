package com.styletheory.android.api.response;

/**
 * Created by stephen on 12/15/15.
 */
public class ProductResponseData {
    public Product[] products;

    public static class Product {
        public int id;
        public String name;
        public String designer;
        public String thumbnail;
        public String retail_price;
        public boolean wishlist;
    }
}
