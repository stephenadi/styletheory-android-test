package com.styletheory.android.api.request;

/**
 * Created by stephen on 12/15/15.
 */
public class WishlistRequestData {
    public int product_id;

    public WishlistRequestData(int product_id) {
        this.product_id = product_id;
    }
}
