package com.styletheory.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by stephen on 11/27/15.
 */
public class FlauntActivity extends BaseActivity {
    private Button mContinueButton;
    private TextView mBackTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_flaunt);

        setUpView();
        setUpListener();
    }

    private void setUpView() {
        mContinueButton = (Button) findViewById(R.id.button_continue);
        mBackTextView = (TextView) findViewById(R.id.text_view_back);
    }

    private void setUpListener() {
        mBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FlauntActivity.this, BodySizeActivity.class);
                startActivity(i);
            }
        });
    }
}
