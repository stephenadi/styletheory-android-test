package com.styletheory.android;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.styletheory.android.api.ApiRoutes;
import com.styletheory.android.api.base.VolleyAPI;
import com.styletheory.android.api.request.LoginRequestData;
import com.styletheory.android.api.response.LoginResponseData;
import com.styletheory.android.ui.form.CustomEditText;
import com.styletheory.android.ui.form.FormContract;
import com.styletheory.android.util.LifecycleUtil;
import com.styletheory.android.util.PhoneDataUtil;
import com.styletheory.android.util.PreferencesUtil;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by stephen on 11/26/15.
 */
public class LoginActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    /* RequestCode for resolutions involving sign-in */
    private static final int RC_SIGN_IN = 1;

    /* RequestCode for resolutions to get GET_ACCOUNTS permission on M */
    private static final int RC_PERM_GET_ACCOUNTS = 2;
    private TextView mGoogleLoginButton;
    private TextView mFbLoginButton;
    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;

    /* Should we automatically resolve ConnectionResults when possible? */
    private boolean mShouldResolve = false;
    private GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;
    private Button mLoginButton;
    private TextView mSkipTextView;
    private TextView mBackTextView;
    private ArrayList<FormContract> mFormFields;
    private CustomEditText mEmailEditText;
    private CustomEditText mPasswordEditText;
    private VolleyAPI<LoginRequestData, LoginResponseData> mLoginApi;
    private ProgressDialog mLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        setUpAttribute();
        setUpView();
        setUpRequestAPI();
        setUpListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        mEmailEditText.requestFocus();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                LifecycleUtil.showKeyboard(mContext, mEmailEditText);
            }
        }, 200);
    }

    private void setUpAttribute() {
        mFormFields = new ArrayList<FormContract>();
    }

    private void setUpView() {
        mGoogleLoginButton = (TextView) findViewById(R.id.text_view_sign_in_google);
        mFbLoginButton = (TextView) findViewById(R.id.text_view_sign_in_facebook);
        mLoginButton = (Button) findViewById(R.id.button_continue);
        mPasswordEditText = (CustomEditText) findViewById(R.id.edit_text_password);
        mEmailEditText = (CustomEditText) findViewById(R.id.edit_text_email);

        mSkipTextView = (TextView) findViewById(R.id.text_view_skip);
        mBackTextView = (TextView) findViewById(R.id.text_view_back);

        mFormFields.add(mEmailEditText);
        mFormFields.add(mPasswordEditText);
    }

    private void setUpRequestAPI() {
        mLoginApi = new VolleyAPI<LoginRequestData, LoginResponseData>(mContext) {
            @Override
            public void onRequestSuccess(LoginResponseData responseData) {
                mLoadingDialog.dismiss();
                PreferencesUtil.setToken(mContext, responseData.access_token);
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
            }

            @Override
            public void onRequestError(VolleyError error) {
                mLoadingDialog.dismiss();
                Toast.makeText(mContext, "No Internet Connection, please try again later", Toast.LENGTH_LONG).show();
            }
        };
    }

    private void setUpListener() {
        mSkipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        mBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allFormValid()) {
                    LoginRequestData requestData = new LoginRequestData();
                    requestData.client_id = PhoneDataUtil.getDeviceId(mContext);
                    requestData.client_secret = PhoneDataUtil.getDeviceSecret(mContext);
                    requestData.username = mEmailEditText.getValue();
                    requestData.password = mPasswordEditText.getValue();
                    requestData.grant_type = "password";
                    requestData.response_type = "token";

                    mLoadingDialog = ProgressDialog.show(LoginActivity.this, "Loading", "Signing in...", true);
                    mLoginApi.startRequest(ApiRoutes.LOGIN_URL, Constans.RequestType.POST, requestData);
                }
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(new Scope(Scopes.PROFILE))
                .addScope(new Scope(Scopes.EMAIL))
                .build();

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(i);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        mFbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Collection<String> collection = new ArrayList<String>();
                collection.add("public_profile");
                collection.add("user_friends");
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, collection);
            }
        });

        mGoogleLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mShouldResolve = true;
                mGoogleApiClient.connect();
                Toast.makeText(LoginActivity.this, "Signing in with google", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

//        mGoogleApiClient.connect(); //enable this will make auto login for google
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            // If the error resolution was not successful we should not resolve further.
            if (resultCode != RESULT_OK) {
                mShouldResolve = false;
            }

            mIsResolving = false;
            mGoogleApiClient.connect();
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);

        if (!mIsResolving && mShouldResolve) {
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(this, RC_SIGN_IN);
                    mIsResolving = true;
                } catch (IntentSender.SendIntentException e) {
                    Log.e(TAG, "Could not resolve ConnectionResult.", e);
                    mIsResolving = false;
                    mGoogleApiClient.connect();
                }
            } else {
                // Could not resolve the connection result, show the user an
                // error dialog.
                Toast.makeText(this, connectionResult.toString(), Toast.LENGTH_LONG).show();
                showErrorDialog(connectionResult);
            }
        } else {
            // Show the signed-out UI
//            showSignedOutUI();
        }
    }

    private void showErrorDialog(ConnectionResult connectionResult) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, RC_SIGN_IN,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                mShouldResolve = false;
                            }
                        }).show();
            } else {
                Log.w(TAG, "Google Play Services Error:" + connectionResult);
                String errorString = apiAvailability.getErrorString(resultCode);
                Toast.makeText(this, errorString, Toast.LENGTH_SHORT).show();

                mShouldResolve = false;
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected:" + bundle);
        mShouldResolve = false;
        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            String personName = currentPerson.getDisplayName();
            String personPhoto = currentPerson.getImage().getUrl();
            String personGooglePlusProfile = currentPerson.getUrl();
            String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
            Toast.makeText(this, personName + ", " + email, Toast.LENGTH_LONG).show();
            Log.d(TAG, "personName : " + personName);
            Log.d(TAG, "personPhoto : " + personPhoto);
            Log.d(TAG, "personGooglePlusProfile : " + personGooglePlusProfile);
            Log.d(TAG, "email : " + email);
        } else {
            Log.d(TAG, "no data");
            Toast.makeText(this, "no data", Toast.LENGTH_LONG).show();
        }

        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w(TAG, "onConnectionSuspended:" + i);
    }

    private boolean allFormValid() {
        for (FormContract formContract : mFormFields) {
            if (formContract.getValue() == null || formContract.getValue().isEmpty()) {
                Toast.makeText(this, "Please complete all form", Toast.LENGTH_LONG).show();
                return false;
            }
        }

        if (mPasswordEditText.getValue().length() < 6) {
            Toast.makeText(this, "Minimal length for password is 6 characters", Toast.LENGTH_LONG).show();
            return false;
        }

        boolean isEmailValid = isValidEmail(mEmailEditText.getValue());
        if (!isEmailValid) {
            Toast.makeText(this, "Email is invalid", Toast.LENGTH_LONG).show();
        }
        return isEmailValid;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
