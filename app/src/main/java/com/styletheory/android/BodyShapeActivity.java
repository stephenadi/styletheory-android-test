package com.styletheory.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by stephen on 11/27/15.
 */
public class BodyShapeActivity extends BaseActivity {
    private Button mContinueButton;
    private TextView mBackTextView;
    private String selectedType;
    private LinearLayout mAppleBodyShapeFrame;
    private LinearLayout mPearBodyShapeFrame;
    private LinearLayout mHourglassBodyShapeFrame;
    private LinearLayout mSlenderBodyShapeFrame;
    private LinearLayout mHeartBodyShapeFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_body_shape);

        setUpView();
        setUpListener();
    }

    private void setUpView() {
        mContinueButton = (Button) findViewById(R.id.button_continue);
        mBackTextView = (TextView) findViewById(R.id.text_view_back);
        mAppleBodyShapeFrame = (LinearLayout) findViewById(R.id.frame_body_shape_apple);
        mPearBodyShapeFrame = (LinearLayout) findViewById(R.id.frame_body_shape_pear);
        mHourglassBodyShapeFrame = (LinearLayout) findViewById(R.id.frame_body_shape_hourglass);
        mSlenderBodyShapeFrame = (LinearLayout) findViewById(R.id.frame_body_shape_slender);
        mHeartBodyShapeFrame = (LinearLayout) findViewById(R.id.frame_body_shape_heart);
    }

    private void setUpListener() {
        mAppleBodyShapeFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uncheckedAllFrame();
                selectedType = "apple";
                mAppleBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_pink_rounded_border);
            }
        });
        mPearBodyShapeFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uncheckedAllFrame();
                selectedType = "pear";
                mPearBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_pink_rounded_border);
            }
        });
        mHourglassBodyShapeFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uncheckedAllFrame();
                selectedType = "hourglass";
                mHourglassBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_pink_rounded_border);
            }
        });
        mSlenderBodyShapeFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uncheckedAllFrame();
                selectedType = "slender";
                mSlenderBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_pink_rounded_border);
            }
        });
        mHeartBodyShapeFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uncheckedAllFrame();
                selectedType = "heart";
                mHeartBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_pink_rounded_border);
            }
        });

        mBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectedType == null){
                    Toast.makeText(mContext, "Please select body type",Toast.LENGTH_LONG).show();
                } else {
                    Intent i = new Intent(BodyShapeActivity.this, FlauntActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    private void uncheckedAllFrame() {
        mAppleBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_gray_rounded_border);
        mPearBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_gray_rounded_border);
        mHourglassBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_gray_rounded_border);
        mSlenderBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_gray_rounded_border);
        mHeartBodyShapeFrame.setBackgroundResource(R.drawable.shape_rect_transparent_gray_rounded_border);
    }
}
