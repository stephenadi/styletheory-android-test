package com.styletheory.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.styletheory.android.entity.IntroEntity;
import com.viewpagerindicator.CirclePageIndicator;

/**
 * Created by stephen on 12/2/15.
 */
public class IntroActivity extends BaseActivity {
    private static final String PARAM_IMAGE = "param_image";

    private TextView mBackTextView;
    private TextView mSkipTextView;
    private CirclePageIndicator mIndicator;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);

        setUpView();
        setUpViewState();
        setUpListener();
    }

    private void setUpView() {
        mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mViewPager = (ViewPager) findViewById(R.id.view_pager_image_header);

        mSkipTextView = (TextView) findViewById(R.id.text_view_skip);
        mBackTextView = (TextView) findViewById(R.id.text_view_back);
    }

    private void setUpViewState() {
        IntroEntity[] imageList = new IntroEntity[4];
        imageList[0] = new IntroEntity();
        imageList[0].image = R.drawable.icon_intro_1;
        imageList[0].title = "BROWSE";
        imageList[0].description = "Browse our wardrobe full of designer outfits";

        imageList[1] = new IntroEntity();
        imageList[1].image = R.drawable.icon_intro_2;
        imageList[1].title = "CHOOSE";
        imageList[1].description = "Pick 3 clothes. We will deliver them to your door";

        imageList[2] = new IntroEntity();
        imageList[2].image = R.drawable.icon_intro_3;
        imageList[2].title = "WEAR";
        imageList[2].description = "Love your new outfit! Keep them as long as you want";

        imageList[3] = new IntroEntity();
        imageList[3].image = R.drawable.icon_intro_4;
        imageList[3].title = "RETURN";
        imageList[3].description = "Return anytime and get your next styles in days";

        final ImageAdapter adapter = new ImageAdapter(mContext, getSupportFragmentManager(), imageList);
        mViewPager.setAdapter(adapter);

        mIndicator.setViewPager(mViewPager);
        if(adapter.getCount() <= 1){
            mIndicator.setVisibility(View.GONE);
        }
    }

    private void setUpListener() {
        mSkipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(IntroActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

        mBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    class ImageAdapter extends FragmentPagerAdapter {
        private final IntroEntity[] mImageList;

        ImageAdapter(Context mContext, FragmentManager fm, IntroEntity[] imageList) {
            super(fm);
            mImageList = imageList;
        }

        @Override
        public Fragment getItem(int i) {
            Fragment hotelImageFragment = new ImageFragment();
            Bundle b = new Bundle();
            b.putParcelable(PARAM_IMAGE, mImageList[i]);
            hotelImageFragment.setArguments(b);
            return hotelImageFragment;
        }

        @Override
        public int getCount() {
            return mImageList.length;
        }
    }

    public static class ImageFragment extends Fragment {
        protected View mLayoutView;
        private ImageView mImageView;
        private TextView descTV;
        private TextView titleTV;
        private IntroEntity mEntity;

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mEntity = (IntroEntity) getArguments().getParcelable(PARAM_IMAGE);
        }

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            mLayoutView = inflater.inflate(R.layout.item_intro, container, false);

            setUpView();

            return mLayoutView;
        }

        private void setUpView() {
            mImageView = (ImageView) mLayoutView.findViewById(R.id.image_view_intro);
            descTV = (TextView) mLayoutView.findViewById(R.id.text_view_description);
            titleTV = (TextView) mLayoutView.findViewById(R.id.text_view_title);
            mImageView.setImageDrawable(getActivity().getResources().getDrawable(mEntity.image));
            titleTV.setText(mEntity.title);
            descTV.setText(mEntity.description);
        }

    }

    public class IntroAdapter extends PagerAdapter {
        private final Context mContext;
        private final LayoutInflater mInflater;
        IntroEntity[] mImageList;

        public IntroAdapter(Context context, FragmentManager fm, IntroEntity[] imageList) {
            super();
            mContext = context;
            mImageList = imageList;
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public Object instantiateItem(ViewGroup collection, int position) {
            LinearLayout introLayout = (LinearLayout) mInflater.inflate(R.layout.item_intro, collection, false);
            ImageView view = (ImageView) introLayout.findViewById(R.id.image_view_intro);
            TextView descTV = (TextView) introLayout.findViewById(R.id.text_view_description);
            TextView titleTV = (TextView) introLayout.findViewById(R.id.text_view_title);
            view.setImageDrawable(mContext.getResources().getDrawable(mImageList[position].image));
            titleTV.setText(mImageList[position].title);
            descTV.setText(mImageList[position].description);

            ((ViewPager) collection).addView(introLayout, 0);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View collection, Object object) {
            return collection == ((View) object);
        }

        @Override
        public int getCount() {
            if (mImageList != null) {
                return mImageList.length;
            } else {
                return 0;
            }
        }
    }
}
