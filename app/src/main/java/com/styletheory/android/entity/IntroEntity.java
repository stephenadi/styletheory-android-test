package com.styletheory.android.entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by stephen on 12/11/15.
 */
public class IntroEntity implements Parcelable {
    public String title;
    public String description;
    public int image;

    public IntroEntity() {
    }

    protected IntroEntity(Parcel in) {
        title = in.readString();
        description = in.readString();
        image = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(description);
        dest.writeInt(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<IntroEntity> CREATOR = new Creator<IntroEntity>() {
        @Override
        public IntroEntity createFromParcel(Parcel in) {
            return new IntroEntity(in);
        }

        @Override
        public IntroEntity[] newArray(int size) {
            return new IntroEntity[size];
        }
    };
}
