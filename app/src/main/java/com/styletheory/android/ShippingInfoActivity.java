package com.styletheory.android;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.styletheory.android.ui.form.CustomEditText;
import com.styletheory.android.ui.form.FormContract;

import java.util.ArrayList;

/**
 * Created by stephen on 11/27/15.
 */
public class ShippingInfoActivity extends BaseActivity {
    private Button mContinueButton;
    private TextView mSkipTextView;
    private TextView mBackTextView;
    private ArrayList<FormContract> mFormFields;
    private CustomEditText mPhoneEditText;
    private CustomEditText mAddressEditText;
    private CustomEditText mCityEditText;
    private CustomEditText mZipEditText;
    private CustomEditText mFirstNameEditText;
    private CustomEditText mLastNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_shipping_info);

        setUpAttribute();
        setUpView();
        setUpListener();
    }

    public BitmapDrawable writeOnDrawable(int drawableId, String text){
        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLACK);
        paint.setTextSize(20);

        Canvas canvas = new Canvas(bm);
        canvas.drawText(text, 0, bm.getHeight()/2, paint);

        return new BitmapDrawable(bm);
    }

    private void setUpAttribute() {
        mFormFields = new ArrayList<FormContract>();
    }

    private void setUpView() {
        mContinueButton = (Button) findViewById(R.id.button_continue);

        mSkipTextView = (TextView) findViewById(R.id.text_view_skip);
        mBackTextView = (TextView) findViewById(R.id.text_view_back);
        mFirstNameEditText = (CustomEditText) findViewById(R.id.edit_text_first_name);
        mLastNameEditText = (CustomEditText) findViewById(R.id.edit_text_last_name);
        mCityEditText = (CustomEditText) findViewById(R.id.edit_text_city);
        mZipEditText = (CustomEditText) findViewById(R.id.edit_text_zip);
        mAddressEditText = (CustomEditText) findViewById(R.id.edit_text_address);
        mPhoneEditText = (CustomEditText) findViewById(R.id.edit_text_phone_number);

//        Drawable d = writeOnDrawable(R.drawable.icon_singapore, "+65");
//        mPhoneEditText.setCompoundDrawablesWithIntrinsicBounds(d, null, null, null);

        mFormFields.add(mFirstNameEditText);
        mFormFields.add(mLastNameEditText);
        mFormFields.add(mCityEditText);
        mFormFields.add(mZipEditText);
        mFormFields.add(mAddressEditText);
        mFormFields.add(mPhoneEditText);
    }

    private void setUpListener() {
        mSkipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ShippingInfoActivity.this, BodyShapeActivity.class);
                startActivity(i);
            }
        });

        mBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(allFormValid()) {
                    Intent i = new Intent(ShippingInfoActivity.this, BodyShapeActivity.class);
                    startActivity(i);
                }
            }
        });
    }

    private boolean allFormValid() {
        for(FormContract formContract : mFormFields){
            if(formContract.getValue() == null || formContract.getValue().isEmpty()){
                Toast.makeText(this, "Please complete all form", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        return true;
    }
}
