package com.styletheory.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.styletheory.android.ui.form.SizeLayout;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;

/**
 * Created by stephen on 11/27/15.
 */
public class BodySizeActivity extends BaseActivity {
    private Button mContinueButton;
    private SizeLayout mBodyTypeFrame;
    private TextView mBackTextView;
    private TextView mSkipTextView;
    private TextView mHeightTextView;
    private TextView mWeightTextView;
    private DiscreteSeekBar mHeightSeekbar;
    private DiscreteSeekBar mWeightSeekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_body_size);

        setUpView();
        setUpViewState();
        setUpListener();
    }

    private void setUpView() {
        mContinueButton = (Button) findViewById(R.id.button_continue);
        mBodyTypeFrame = (SizeLayout) findViewById(R.id.frame_body_type);

        mSkipTextView = (TextView) findViewById(R.id.text_view_skip);
        mBackTextView = (TextView) findViewById(R.id.text_view_back);
        mWeightTextView = (TextView) findViewById(R.id.text_view_current_weight);
        mHeightTextView = (TextView) findViewById(R.id.text_view_current_height);
        mHeightSeekbar = (DiscreteSeekBar) findViewById(R.id.seekbar_height);
        mWeightSeekbar = (DiscreteSeekBar) findViewById(R.id.seekbar_weight);
    }

    private void setUpViewState() {
        ArrayList<String> tops = new ArrayList<>();
        tops.add("OUVAL");
        tops.add("CURVY");
        tops.add("BOLD");
        mBodyTypeFrame.inflateLayout("bodyType", tops, true);
    }

    private void setUpListener() {
        mWeightSeekbar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                mWeightTextView.setText(Integer.toString(value));
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });

        mHeightSeekbar.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                mHeightTextView.setText(Integer.toString(value));
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

            }
        });

        mSkipTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BodySizeActivity.this, FitSizeActivity.class);
                startActivity(i);
            }
        });

        mBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mBodyTypeFrame.getValue() == null){
                    Toast.makeText(mContext, "Please select body type", Toast.LENGTH_LONG).show();
                } else {
                    Intent i = new Intent(BodySizeActivity.this, FitSizeActivity.class);
                    startActivity(i);
                }
            }
        });
    }
}
