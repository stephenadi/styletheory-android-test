package com.styletheory.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andtinder.model.CardModel;
import com.andtinder.view.CardStackAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.styletheory.android.R;
import com.styletheory.android.StyleTheoryApplication;

/**
 * Created by stephen on 11/3/15.
 */
public final class CustomCardAdapter extends CardStackAdapter {
    private final ImageLoader mImageLoader;

    public CustomCardAdapter(Context mContext) {
        super(mContext);
        mImageLoader = ((StyleTheoryApplication) mContext.getApplicationContext()).getImageLoader();
    }

    public View getCardView(int position, CardModel model, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(this.getContext());
            convertView = inflater.inflate(R.layout.item_card, parent, false);

            assert convertView != null;
        }

        mImageLoader.displayImage("http://antimatter15.com/weppy/tinybrot.webp", (ImageView)convertView.findViewById(R.id.imageview));

        if(model.isOnLike()){
            ((ImageView)convertView.findViewById(R.id.image_view_like)).setVisibility(View.VISIBLE);
        } else {
            ((ImageView)convertView.findViewById(R.id.image_view_like)).setVisibility(View.GONE);
        }

        if(model.isOnDislike()){
            ((ImageView)convertView.findViewById(R.id.image_view_dislike)).setVisibility(View.VISIBLE);
        } else {
            ((ImageView)convertView.findViewById(R.id.image_view_dislike)).setVisibility(View.GONE);
        }
        ((TextView)convertView.findViewById(R.id.title)).setText(model.getTitle());
        ((TextView)convertView.findViewById(R.id.description)).setText(model.getDescription());
        return convertView;
    }
}