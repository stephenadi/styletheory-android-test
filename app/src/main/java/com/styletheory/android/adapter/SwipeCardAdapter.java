package com.styletheory.android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.styletheory.android.R;
import com.styletheory.android.StyleTheoryApplication;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by stephen on 11/9/15.
 */
public class SwipeCardAdapter extends ArrayAdapter<String> {
    private final ImageLoader mImageLoader;
    private final ArrayList<String> mData;
    private final ArrayList<String> mRealData;

    public SwipeCardAdapter(Context context, int resource, String[] strings) {
        super(context, resource);
        mImageLoader = ((StyleTheoryApplication) context.getApplicationContext()).getImageLoader();
        mData = new ArrayList<String>(Arrays.asList(strings));
        mRealData = new ArrayList<String>(Arrays.asList(strings));
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int position) {
        return mData.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(this.getContext());
            convertView = inflater.inflate(R.layout.item_card, parent, false);
        }
//        mImageLoader.displayImage("http://antimatter15.com/weppy/tinybrot.webp", (ImageView)convertView.findViewById(R.id.imageview));
        ((TextView)convertView.findViewById(R.id.title)).setText(getItem(position));

        return convertView;
    }

    public void removeFirstObject() {
        mData.remove(0);
    }

    public boolean undo() {
        int index = mRealData.size() - mData.size() - 1;
        if(index != -1) {
            mData.add(0, mRealData.get(index));
            notifyDataSetChanged();
            return true;
        } else {
            return false;
        }
    }
}
