package com.styletheory.android.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.styletheory.android.R;
import com.styletheory.android.StyleTheoryApplication;

/**
 * Created by stephen on 11/24/15.
 */

public class ImageAdapter extends PagerAdapter {
    private static final String PARAM_BANNER = "param_image_url";
    private final Context mContext;
    private final ImageLoader mImageLoader;
    String[] mImageList;

    public ImageAdapter(Context context, FragmentManager fm, String[] imageList) {
        super();
        mContext = context;
        mImageList = imageList;
        mImageLoader = StyleTheoryApplication.getImageLoader();
    }
//
//    @Override
//    public Fragment getItem(int i) {
//        Fragment hotelImageFragment = new BannerImageFragment();
//        Bundle b = new Bundle();
//        b.putString(PARAM_BANNER, mImageList[i]);
//        hotelImageFragment.setArguments(b);
//        return hotelImageFragment;
//    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = (LayoutInflater) collection.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ImageView view = new ImageView(mContext);
//        mImageLoader.displayImage("http://antimatter15.com/weppy/tinybrot.webp", view);
        mImageLoader.displayImage(mImageList[position], view);

        ((ViewPager) collection).addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View collection, Object object) {
        return collection == ((View) object);
    }

    @Override
    public int getCount() {
        if(mImageList != null) {
            return mImageList.length;
        } else{
            return 0;
        }
    }

    public static class BannerImageFragment extends Fragment {
        String mImage;
        protected View mLayoutView;
        private ProgressBar mLoadingProgress;
        private ImageView mImageView;
        private ImageLoader mImageLoader;

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            mLayoutView = inflater.inflate(R.layout.fragment_product_image, container, false);
            mImage = getArguments().getString(PARAM_BANNER);
            mImageLoader = StyleTheoryApplication.getImageLoader();
            setUpView();
            return mLayoutView;
        }

        private void setUpView() {
            mImageView = (ImageView) mLayoutView.findViewById(R.id.image_view_gallery);
            mLoadingProgress = (ProgressBar) mLayoutView.findViewById(R.id.loading_animation);

            if(mImageView.getDrawable() == null){
                loadImage();
            } else{
                ProgressBar loadingProgress = (ProgressBar) mLayoutView.findViewById(R.id.loading_animation);
                loadingProgress.setVisibility(View.INVISIBLE);//harus di findVieById lagi ga tau knp bru bisa di buat invisible
            }
        }

        private void loadImage() {
            mLoadingProgress.setVisibility(View.VISIBLE);
            Log.d("test image", mImage);

            mImageLoader.displayImage("http://antimatter15.com/weppy/tinybrot.webp", mImageView);
        }
    }
}




