package com.styletheory.android;

import android.content.ClipData;
import android.content.ClipDescription;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.facebook.FacebookSdk;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.styletheory.android.ui.TouchImageView;


/**
 * Created by stephen on 11/3/15.
 */
public class DressDetailActivity extends AppCompatActivity {
    private LinearLayout mMainFrame;

    private static com.nostra13.universalimageloader.core.ImageLoader mImageLoader;
    private ViewPager mGalleryFrame;
    private static ImageView mGalleryImageView;
    private ImageView mSmallImageView;
    private static boolean mOnDrag;
    private static float mInitialY;
    //    private LinearLayout mImageContainerFrame;
    private FrameLayout mParentLayout;
    private int mMiddlePoint;
    private ScrollView mScrollView;
    private float mLastX;
    private float mLastY;
    private int _yDelta;
    private int mInitialHeight;
    private int mInitialWidth;
    private SwipeFlingAdapterView mFrame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_dress_detail);
        mImageLoader = StyleTheoryApplication.getImageLoader();

        setUpAttribute();
        setUpView();
        setUpListener();
    }

    private void setUpAttribute() {
        mOnDrag = false;
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        mMiddlePoint = height / 2;
    }

    private void setUpListener() {
        mGalleryFrame.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        mGalleryFrame.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        mGalleryFrame.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        mSmallImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSmallImageView.setVisibility(View.INVISIBLE);
                mGalleryFrame.setVisibility(View.VISIBLE);
            }
        });

        mMainFrame.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent dragEvent) {
                switch (dragEvent.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        Log.d("test drag", "ACTION_DRAG_STARTED");
                        if (dragEvent.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {//uncomment for drag along Y axis only
                            mOnDrag = true;
                            ViewGroup.MarginLayoutParams lParams = (ViewGroup.MarginLayoutParams) mGalleryImageView.getLayoutParams();
                            _yDelta = (int) dragEvent.getY() - lParams.topMargin;
                            return true;
                        }
                        return false;
                    case DragEvent.ACTION_DRAG_LOCATION:
                        Log.d("test drag", dragEvent.getX() + "," + (dragEvent.getY() - mScrollView.getScrollY()));
                        Log.d("test drag", "alpha:" + (1 - (Math.abs(mMiddlePoint - (dragEvent.getY() - mScrollView.getScrollY())) / mMiddlePoint)));
                        mGalleryFrame.setAlpha(1 - (Math.abs(mMiddlePoint - (dragEvent.getY() - mScrollView.getScrollY())) / mMiddlePoint));
//uncomment for drag along Y axis only
                        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) mGalleryImageView.getLayoutParams();
                        layoutParams.topMargin = (int) dragEvent.getY() - _yDelta;
                        mGalleryImageView.setLayoutParams(layoutParams);
                        return true;
                    case DragEvent.ACTION_DROP:
                        Log.d("test drag", "ACTION_DROP, alpha : " + mGalleryFrame.getAlpha());
                        if (mGalleryFrame.getAlpha() > 0.7) {
                            return false;
                        }
                        mLastX = dragEvent.getX() /*+ (mGalleryImageView.getLayoutParams().width/2)*/;
                        mLastY = dragEvent.getY() /*+ (mGalleryImageView.getLayoutParams().height/2)*/;
                        mInitialWidth = mGalleryImageView.getLayoutParams().width;
                        mInitialHeight = mGalleryImageView.getLayoutParams().height;
                        Log.d("test drag", "last x : " + mLastX + ", last y : " + mLastY);

                        return true;
                    case DragEvent.ACTION_DRAG_ENDED:
                        mOnDrag = false;
                        mGalleryFrame.setAlpha(1);
                        if (dragEvent.getResult()) {
                            Log.d("test drag", "ACTION_DRAG_ENDED");
                            mGalleryFrame.setVisibility(View.GONE);
                            mSmallImageView.setVisibility(View.VISIBLE);//if there is more than 1 small image, then set mSmallImageView dynamically
                            mGalleryImageView.setVisibility(View.VISIBLE);

                            int currentPost = mGalleryFrame.getCurrentItem();
                            mGalleryFrame.setAdapter(new ImageAdapter(getSupportFragmentManager()));
                            mGalleryFrame.setCurrentItem(currentPost);

//                            float oldX = mSmallImageView.getX();
//                            float oldY = mSmallImageView.getY();
//                            mSmallImageView.setX(mLastX);
//                            mSmallImageView.setY(mLastY);
//                            ObjectAnimator xAnimation = ObjectAnimator.ofFloat(mSmallImageView, "x", oldX);
//                            ObjectAnimator yAnimation = ObjectAnimator.ofFloat(mSmallImageView, "y", oldY);
//                            AnimatorSet animatorSet = new AnimatorSet();
//                            animatorSet.play(xAnimation).with(yAnimation);
//                            animatorSet.setDuration(2000);
//                            animatorSet.start();
                        } else {
                            int currentPost = mGalleryFrame.getCurrentItem();
                            mGalleryFrame.setAdapter(new ImageAdapter(getSupportFragmentManager()));
                            mGalleryFrame.setCurrentItem(currentPost);//force redraw the view pager, if not the image will be missing
                        }
                        return true;
                    default:
                        return false;
                }
            }
        });


    }

    private void setUpView() {
        mMainFrame = (LinearLayout) findViewById(R.id.frame_main);

//        mImageContainerFrame = (LinearLayout) findViewById(R.id.frame_image_container);
        mSmallImageView = (ImageView) findViewById(R.id.imageview);
        mGalleryFrame = (ViewPager) findViewById(R.id.frame_gallery);
        mParentLayout = (FrameLayout) findViewById(R.id.parent_layout);
        mScrollView = (ScrollView) findViewById(R.id.scrollview);

        mImageLoader.displayImage("http://antimatter15.com/weppy/tinybrot.webp", mSmallImageView);

        mGalleryFrame.setAdapter(new ImageAdapter(getSupportFragmentManager()));
    }


    class ImageAdapter extends FragmentPagerAdapter {
        ImageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            Fragment hotelImageFragment = new HotelImageFragment();
            Bundle b = new Bundle();
            hotelImageFragment.setArguments(b);
            return hotelImageFragment;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }

    public static class HotelImageFragment extends Fragment {
        protected View mLayoutView;
        private TouchImageView mImageView;
        private float mLastY;
        private boolean mCurrentlyIsDown;

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            super.onCreateView(inflater, container, savedInstanceState);
            mLayoutView = inflater.inflate(R.layout.fragment_image_gallery, container, false);

            setUpView();
            setUpListener();

            return mLayoutView;
        }

        private void setUpView() {
            mImageView = (TouchImageView) mLayoutView.findViewById(R.id.image_view_gallery);
            loadImage();
        }

        private void loadImage() {
            mImageLoader.displayImage("http://antimatter15.com/weppy/tinybrot.webp", mImageView, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    loadImage();
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//                    mLayoutView.findViewById(R.id.progress_bar).setVisibility(View.GONE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });
        }

        private void setUpListener() {
            mImageView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(final View view, MotionEvent motionEvent) {
                    Log.d("Test drag", "action: " + motionEvent.getAction());
                    if (mImageView.getCurrentZoom() == mImageView.getMinZoom() && motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                        Log.d("Test drag", "selisih:" + (motionEvent.getRawY() - mLastY));
                        if (!mOnDrag && ((motionEvent.getRawY() - mLastY) > 40 || (motionEvent.getRawY() - mLastY) < -40)) {
                            Log.d("Test drag", "valid for drag");
                            mGalleryImageView = mImageView;
//                            mOnDrag = true;

                            int[] location = new int[2];
                            mImageView.getLocationOnScreen(location);
                            mInitialY = location[1];

                            final ClipData clipData = ClipData.newPlainText("", "");
//                            final View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
                            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(new View(getActivity()));//uncomment to chnge to up down only

                            mImageView.resetZoom();
                            mImageView.startDrag(clipData, shadowBuilder, view, 0);
//                            mImageView.setVisibility(View.INVISIBLE);//comment to chnge to up down only
                            return true;//return true mean the other touch listener wont get a chance to process it
                        } else {
                            Log.d("Test drag", "invalid for drag, mOndrag : " + mOnDrag);
                        }
                    } else if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        mCurrentlyIsDown = false;
                        mLastY = motionEvent.getRawY();
                        Log.d("Test drag", "down y:" + motionEvent.getRawY());
                    }

                    return false;//return false mean the other touch listener may get chance to process it
                }
            });
        }
    }
}
