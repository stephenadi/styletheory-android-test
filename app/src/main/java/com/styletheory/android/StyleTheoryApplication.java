package com.styletheory.android;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonElement;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.styletheory.android.api.ApiRoutes;
import com.styletheory.android.api.base.RequestManager;
import com.styletheory.android.api.base.VolleyAPI;
import com.styletheory.android.api.request.AppInstanceRequestData;
import com.styletheory.android.util.PhoneDataUtil;

import java.io.File;

import io.fabric.sdk.android.Fabric;

/**
 * Created by stephen on 11/9/15.
 */
public class StyleTheoryApplication extends Application {
    private static ImageLoader mImageLoader;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(getBaseContext());
        RequestManager.init(this);
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);

        requestAppInstance();
//        DiskCacheConfig diskCacheConfig = DiskCacheConfig.newBuilder().setBaseDirectoryPath(getExternalDirectory(this, "cache"))
//                .setBaseDirectoryName("v1")
//                .setMaxCacheSize(10 * ByteConstants.MB)
//                .setMaxCacheSizeOnLowDiskSpace(10 * ByteConstants.MB)
//                .setMaxCacheSizeOnVeryLowDiskSpace(5 * ByteConstants.MB)
//                .setVersion(1)
//                .build();
//
//        ImagePipelineConfig imagePipelineConfig = ImagePipelineConfig.newBuilder(this)
//                .setMainDiskCacheConfig(diskCacheConfig).build();
//
//        Fresco.initialize(this, imagePipelineConfig);

        File cacheDir = StorageUtils.getCacheDirectory(this);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .diskCache(new UnlimitedDiscCache(cacheDir)) // default
                .diskCacheSize(100 * 1024 * 1024)
                .diskCacheFileCount(100)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
        mImageLoader = ImageLoader.getInstance();
    }

    private void requestAppInstance() {
        AppInstanceRequestData requestData = new AppInstanceRequestData();
        requestData.app_instance = PhoneDataUtil.getDeviceId(this);
        requestData.operating_system = PhoneDataUtil.getOsVersion();

        new VolleyAPI<AppInstanceRequestData, JsonElement>(this){
            @Override
            public void onRequestSuccess(JsonElement responseData) {

            }
        }.startRequest(ApiRoutes.APP_INSTANCE_URL, Constans.RequestType.POST, requestData);
    }

    public static File getExternalDirectory(Context context, String path){
        File mediaStorageDirectory = context.getExternalFilesDir(File.separator + path);

        if (!mediaStorageDirectory.exists()){
            if (!mediaStorageDirectory.mkdirs()){
                return null;
            }
        }

        return mediaStorageDirectory;
    }

    public static ImageLoader getImageLoader() {
        return mImageLoader;
    }
}
