package com.styletheory.android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.lorentzos.flingswipe.SwipeFlingAdapterView;
import com.styletheory.android.adapter.SwipeCardAdapter;

/**
 * Created by stephen on 11/27/15.
 */
public class SwipeActivity extends BaseActivity {
    private TextView mUndoTextView;
    private SwipeFlingAdapterView mSwipeView;
    private SwipeCardAdapter mAdapter;
    private TextView mItemNumberTextView;
    private int mCardIndex;
    private int mNumCard;
    private TextView mSkipTextView;
    private TextView mBackTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_swipe);

        mCardIndex = 0;
        setUpView();
        setUpSwipeCard();
        setUpListener();
    }

    private void setUpView() {
        mUndoTextView = (TextView) findViewById(R.id.text_view_undo);
        mItemNumberTextView = (TextView) findViewById(R.id.text_view_item_number);
        mBackTextView = (TextView) findViewById(R.id.text_view_back);
    }

    private void setUpListener() {
        mBackTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mUndoTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mAdapter.undo()){
                    mCardIndex--;
                    updateCardNumber();
                    mSwipeView.refresh();
                }
            }
        });
    }

    private void setUpSwipeCard() {
        mSwipeView = (SwipeFlingAdapterView) findViewById(R.id.swipe_adapter_view);
        mAdapter = new SwipeCardAdapter(mContext, R.layout.item_card, new String[]{"a", "b", "c", "d", "e"});
        mSwipeView.setAdapter(mAdapter);
        mNumCard = mAdapter.getCount();

        updateCardNumber();
        mSwipeView.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {
                mAdapter.removeFirstObject();
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                mCardIndex++;
                if (mCardIndex == mNumCard) {
                    Intent i = new Intent(SwipeActivity.this, IntroActivity.class);
                    startActivity(i);
                } else {
                    updateCardNumber();
                }
            }

            @Override
            public void onRightCardExit(Object dataObject) {
                mCardIndex++;
                if (mCardIndex == mNumCard) {
                    Intent i = new Intent(SwipeActivity.this, IntroActivity.class);
                    startActivity(i);
                } else {
                    updateCardNumber();
                }
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {
            }

            @Override
            public void onScroll(float scrollProgressPercent) {
                try {//somtime crash if swipe to quick
                    Log.d("test swipe", "percent : " + scrollProgressPercent);
                    View view = mSwipeView.getSelectedView();
                    view.findViewById(R.id.frame_dislike).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                    view.findViewById(R.id.frame_like).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                } catch (Exception e){}
            }
        });
    }

    private void updateCardNumber() {
        mItemNumberTextView.setText((mCardIndex + 1) + "/" + mNumCard);
    }
}
