package com.styletheory.android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by stephen on 11/26/15.
 */
public class SplashActivity extends BaseActivity {
    private Button mRegisterButton;
    private Button mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        setUpView();
        setUpListener();
    }

    private void setUpView() {
        mRegisterButton = (Button) findViewById(R.id.button_register);
        mLoginButton = (Button) findViewById(R.id.button_login);
    }

    private void setUpListener() {
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SplashActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });
    }
}
