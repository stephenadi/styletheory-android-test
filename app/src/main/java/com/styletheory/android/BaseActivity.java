package com.styletheory.android;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;

/**
 * Created by stephen on 11/12/15.
 */
public class BaseActivity extends AppCompatActivity {

    public String TAG;
    protected String mTitle;

    protected Context mContext;
    protected LayoutInflater mLayoutInflater;

    public BaseActivity() {
        super();

        TAG = "BaseActivity";
    }


    //================================================================================
    // Life Cycle
    //================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        mLayoutInflater = LayoutInflater.from(mContext);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

}

