package com.styletheory.android;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.styletheory.android.api.response.CategoryResponseData;
import com.styletheory.android.api.response.DesignerResponseData;
import com.styletheory.android.container.BaseContainerFragment;
import com.styletheory.android.container.FeatureContainerFragment;
import com.styletheory.android.container.FifthContainerFragment;
import com.styletheory.android.container.FourthContainerFragment;
import com.styletheory.android.container.WardrobeContainerFragment;
import com.styletheory.android.container.WishlistContainerFragment;
import com.styletheory.android.fragments.LeftNavigationDrawerFragment;
import com.styletheory.android.fragments.RightNavigationDrawerFragment;
import com.styletheory.android.ui.CustomTabHost;
import com.styletheory.android.util.AnimationUtil;

/**
 * Created by stephen on 11/12/15.
 */

public class MainActivity extends BaseActivity implements LeftNavigationDrawerFragment.NavigationDrawerCallbacks {
    public static final String FEATURE_SECTION_TAG = "Home";
    public static final String WARDROBE_SECTION_TAG = "Second";
    public static final String THIRD_SECTION_TAG = "Third";
    public static final String FOURTH_SECTION_TAG = "Fourth";
    public static final String FIFTH_SECTION_TAG = "Fifth";

    public static final int FEATURE_SECTION_NUMBER = 0;
    public static final int WARDROBE_SECTION_NUMBER = 1;
    public static final int THIRD_SECTION_NUMBER = 2;
    public static final int FOURTH_SECTION_NUMBER = 3;
    public static final int FIFTH_SECTION_NUMBER = 4;
    //================================================================================
    // Current Activity Variable
    //================================================================================

    public LeftNavigationDrawerFragment mLeftNavigationDrawerFragment;
    private CharSequence mTitle;

    private CustomTabHost mFragmentTabHost;
    private Toolbar mTopToolbar;
    private Toolbar mBottomToolbar;
    private LinearLayout mFirstTab;
    private LinearLayout mSecondTab;
    private LinearLayout mThirdTab;
    private LinearLayout mFourthTab;
    private LinearLayout mFifthTab;
    private float mLastScrollY;
    private int mActionBarSize;
    private RightNavigationDrawerFragment mRightNavigationDrawerFragment;
    private DrawerLayout mDrawerLayout;
    private boolean mIsToolbarHide;
    private LinearLayout mRootFrame;
    private TextView mTitleToolbarTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpAttribute();
        setUpView();
        setSupportActionBar(mTopToolbar);

        setUpTabHost();
        setUpListener();
        setUpNavigationDrawer();
    }

    private void setUpAttribute() {
        final TypedArray styledAttributes = mContext.getTheme().obtainStyledAttributes(
                new int[]{android.R.attr.actionBarSize});
        mActionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();
    }

    private void setUpView() {
        mTopToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        mTitleToolbarTextView = (TextView) mTopToolbar.findViewById(R.id.text_view_title);
        mBottomToolbar = (Toolbar) findViewById(R.id.bottom_toolbar);
        mFirstTab = (LinearLayout) findViewById(R.id.frame_feature);
        mSecondTab = (LinearLayout) findViewById(R.id.frame_wardrobe);
        mThirdTab = (LinearLayout) findViewById(R.id.frame_wishlist);
        mFourthTab = (LinearLayout) findViewById(R.id.frame_subscription);
        mFifthTab = (LinearLayout) findViewById(R.id.frame_profile);
        mRootFrame = (LinearLayout) findViewById(R.id.frame_root);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        Log.d("test scroll", "actionbar size : " + mActionBarSize);
        Log.d("test scroll", "real actionbar size : " + mBottomToolbar.getLayoutParams().height);
    }

    private void setUpListener() {
        mFirstTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchTab(0);
            }
        });
        mSecondTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchTab(1);
            }
        });
        mThirdTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchTab(2);
            }
        });
        mFourthTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchTab(3);
            }
        });
        mFifthTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchTab(4);
            }
        });

//        mNestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                float currentY = mNestedScrollView.getScrollY();
//                if (currentY > mLastScrollY + 50) {
//                    Log.d("test","scroll turun");
//                    if(mBottomToolbar.getTranslationY() == 0) {
//                        Log.d("test","hide toolbar");
//                        mBottomToolbar.setTranslationY(mActionBarSize);
//                        mTopToolbar.setTranslationY(-mActionBarSize);
//                        mLastScrollY = currentY;
//                    }
//                } else if(currentY < mLastScrollY - 50){
//                    Log.d("test","scroll keatas");
//                    if(mBottomToolbar.getTranslationY() >= mActionBarSize) {
//                        Log.d("test","show toolbar");
//                        mBottomToolbar.setTranslationY(0);
//                        mTopToolbar.setTranslationY(0);
//                        mLastScrollY = currentY - 50;
//                    }
//                }
//            }
//        });


//        mNestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
//            @Override
//            public void onScrollChanged() {
//                Log.d("Test scroll2", "top " + mNestedScrollView.getNestedScrollAxes());
//                //need to get top toolbar movement, to add to total scroll Y calculation, because if the toolbar is currently collapsing, the scroll still count as 0
//                //in ios, only detect scroll y when first time action down, then if scroll up is > threshold, show top and bottom toolbar using translateY
//                int actionbarScroll = (int) mNestedScrollView.getNestedScrollAxes();
//                float currentY = mNestedScrollView.getScrollY() + actionbarScroll;
//
//                Log.d("Test scroll2", "current scroll Y: " + currentY);
//
//                if (mLastScrollY != 0) {
//                    Log.d("Test scroll2", "scroll by: " + (currentY - mLastScrollY));
//
//                    if (currentY > mLastScrollY) {//scroll turun
//                        Log.d("Test scroll2", "scroll turun, height awal : " + mBottomToolbar.getLayoutParams().height);
//                        int newHeight = (int) Math.max(0, mBottomToolbar.getLayoutParams().height - (currentY - mLastScrollY));
//                        if (newHeight >= mActionBarSize) {
//                            newHeight = mActionBarSize;
////                            newHeight = ActionBar.LayoutParams.WRAP_CONTENT;
//                        }
//                        mBottomToolbar.getLayoutParams().height = newHeight;
//                    } else {//scroll naik
//                        Log.d("Test scroll2", "scroll naek, height awal : " + mBottomToolbar.getLayoutParams().height);
//                        int newHeight = (int) Math.min(mActionBarSize, mBottomToolbar.getLayoutParams().height - (currentY - mLastScrollY));
//                        if (newHeight < 0) {
//                            newHeight = 0;
//                        } else if (newHeight >= mActionBarSize) {
//                            newHeight = mActionBarSize;
////                            newHeight = ActionBar.LayoutParams.WRAP_CONTENT;
//                        }
//                        mBottomToolbar.getLayoutParams().height = newHeight;
//                    }
//                    mBottomToolbar.requestLayout();
//                    Log.d("Test scroll2", "height sesudah : " + mBottomToolbar.getLayoutParams().height);
//                }
//                mLastScrollY = currentY;
//            }
//        });
    }

    private void setUpTabHost() {
        mFragmentTabHost = (CustomTabHost) findViewById(android.R.id.tabhost);
        mFragmentTabHost.setup(this, getSupportFragmentManager(), R.id.fragment_section_container);

        mFragmentTabHost.addTab(mFragmentTabHost.newTabSpec(FEATURE_SECTION_TAG).setIndicator(FEATURE_SECTION_TAG), FeatureContainerFragment.class, null);
        mFragmentTabHost.addTab(mFragmentTabHost.newTabSpec(WARDROBE_SECTION_TAG).setIndicator(WARDROBE_SECTION_TAG), WardrobeContainerFragment.class, null);
        mFragmentTabHost.addTab(mFragmentTabHost.newTabSpec(THIRD_SECTION_TAG).setIndicator(THIRD_SECTION_TAG), WishlistContainerFragment.class, null);
        mFragmentTabHost.addTab(mFragmentTabHost.newTabSpec(FOURTH_SECTION_TAG).setIndicator(FOURTH_SECTION_TAG), FourthContainerFragment.class, null);
        mFragmentTabHost.addTab(mFragmentTabHost.newTabSpec(FIFTH_SECTION_TAG).setIndicator(FIFTH_SECTION_TAG), FifthContainerFragment.class, null);

        mFragmentTabHost.getTabWidget().setVisibility(View.GONE);

        switchTab(FEATURE_SECTION_NUMBER);
    }

    private void setUpNavigationDrawer() {
        mLeftNavigationDrawerFragment = (LeftNavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mLeftNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout), mTopToolbar);
        mRightNavigationDrawerFragment = (RightNavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_right);
        mRightNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout), null);

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public Toolbar getTopToolbar() {
        return mTopToolbar;
    }

    public Toolbar getBottomToolbar() {
        return mBottomToolbar;
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        switchTab(position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        BaseContainerFragment currentContainerFragment = (BaseContainerFragment) getSupportFragmentManager().findFragmentByTag(this.mFragmentTabHost.getCurrentTabTag());
        FragmentManager childFragmentManager = currentContainerFragment.getChildFragmentManager();
        Fragment currentFragment = childFragmentManager.findFragmentById(currentContainerFragment.getContainerId());
        currentFragment.onActivityResult(requestCode, resultCode, data);
    }

    public void switchTab(int position) {
        switch (position) {
            case FEATURE_SECTION_NUMBER:
                getToolBar().setTitle(FEATURE_SECTION_TAG);
                mFragmentTabHost.setCurrentTabByTag(FEATURE_SECTION_TAG);
                break;
            case WARDROBE_SECTION_NUMBER:
                getToolBar().setTitle(WARDROBE_SECTION_TAG);
                mFragmentTabHost.setCurrentTabByTag(WARDROBE_SECTION_TAG);
                break;
            case THIRD_SECTION_NUMBER:
                getToolBar().setTitle(THIRD_SECTION_TAG);
                mFragmentTabHost.setCurrentTabByTag(THIRD_SECTION_TAG);
                break;
            case FOURTH_SECTION_NUMBER:
                getToolBar().setTitle(FOURTH_SECTION_TAG);
                mFragmentTabHost.setCurrentTabByTag(FOURTH_SECTION_TAG);
                break;
            case FIFTH_SECTION_NUMBER:
                getToolBar().setTitle(FIFTH_SECTION_TAG);
                mFragmentTabHost.setCurrentTabByTag(FIFTH_SECTION_TAG);
                break;
        }
    }

    public void selectItem(int position) {
        switchTab(position);
    }

    public void setDrawerIndicatorEnabled(boolean enabled) {
        mLeftNavigationDrawerFragment.setDrawerIndicatorEnabled(enabled);
    }

    public Toolbar getToolBar() {
        return mTopToolbar;
    }

    @Override
    public void onBackPressed() {
        try {
            Fragment currentContainerFragment = getSupportFragmentManager().findFragmentByTag(this.mFragmentTabHost.getCurrentTabTag());//ambil container yg skrg
            if (currentContainerFragment.getChildFragmentManager().getBackStackEntryCount() > 0) {//klo ada fragment seblmny yg disimpen di stack
                if (currentContainerFragment.getChildFragmentManager().getBackStackEntryCount() == 1) {
                    setDrawerIndicatorEnabled(true);
                }
                currentContainerFragment.getChildFragmentManager().popBackStack();
            } else {
                finish();
            }
        } catch (Exception e){}
    }

    public Fragment getCurrentDisplayedFragment() {
        Fragment currentContainerFragment = getSupportFragmentManager().findFragmentByTag(this.mFragmentTabHost.getCurrentTabTag());//ambil container yg skrg
        Fragment currentFragment = currentContainerFragment.getChildFragmentManager().findFragmentById(R.id.container);
        return currentFragment;
    }

    public String getCurrentDisplayedFragmentTag() {
        Fragment currentContainerFragment = getSupportFragmentManager().findFragmentByTag(this.mFragmentTabHost.getCurrentTabTag());//ambil container yg skrg
        Fragment currentFragment = currentContainerFragment.getChildFragmentManager().findFragmentById(R.id.container);
        return currentFragment.getTag();
    }

    public void hideToolbar() {
        Log.d("test","hide");
        if(!mIsToolbarHide) {
            mIsToolbarHide = true;
            AnimationUtil.collapse(mTopToolbar);
            AnimationUtil.collapse(mBottomToolbar);
        }
    }

    public void showToolbar() {
        if(mIsToolbarHide) {
            mIsToolbarHide = false;
            AnimationUtil.expand(mTopToolbar);
            AnimationUtil.expand(mBottomToolbar);
        }
    }

    public void openRightNavigation() {
        mRightNavigationDrawerFragment.openRightDrawer();
    }

    public void setToolbarTitle(String s){
        mTitleToolbarTextView.setText(s);
    }

    public void setUpLeftNavigation(CategoryResponseData responseData, DesignerResponseData designerResponseData, ExpandableListView.OnGroupClickListener onGroupClickListener) {
        mLeftNavigationDrawerFragment.setUpLeftNavigation(responseData, designerResponseData, onGroupClickListener);
    }
}

